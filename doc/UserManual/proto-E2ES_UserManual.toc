\contentsline {section}{\numberline {1}Download}{4}{section.1}
\contentsline {section}{\numberline {2}Installation}{5}{section.2}
\contentsline {section}{\numberline {3}proto-E2ES\_v1.0}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}auxiliary}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}catalog}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}conf}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}doc}{9}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}src}{9}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}files}{11}{subsection.3.6}
\contentsline {section}{\numberline {4}Usage}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Overall configuration}{12}{subsection.4.1}
\contentsline {section}{\numberline {5}The script}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}\textsf {Euclid\_Survey\_Strategy\_Module\nobreakspace {}(ECSS)}}{17}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}\textsf {Simulated\_Sky\_Module\nobreakspace {}(SS)}}{17}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}\textsf {Spacecraft\_\&\_Environment\_Module\nobreakspace {}(SCE)}}{17}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}\textsf {Optical\_Model\_Module\nobreakspace {}(OM)}}{18}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}\textsf {Detection\_System\_Module\nobreakspace {}(DS)}}{18}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}\textsf {On-Board\_Data\_Generation\_Module\nobreakspace {}(OBDG)}}{19}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}\textsf {Data\_Processing\_\&\_Calibration\_Module\nobreakspace {}(DPC)}}{19}{subsection.5.7}
\contentsline {subsection}{\numberline {5.8}\textsf {Performance\_Assessment\_Module\nobreakspace {}(PA)}}{20}{subsection.5.8}
\contentsline {section}{\numberline {6}The \textsf {.log} file}{22}{section.6}
\contentsline {section}{\numberline {7}Dependencies}{24}{section.7}
\contentsline {section}{\numberline {A}\textsf {OTL\_maker.py}}{26}{appendix.A}
\contentsline {section}{\numberline {B}TIPS installation}{27}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Possible issues}{28}{subsection.B.1}
\contentsline {section}{\numberline {C}IRAF installation}{29}{appendix.C}
\contentsline {subsection}{\numberline {C.1}IRAF on Debian based systems}{29}{subsection.C.1}
\contentsline {subsection}{\numberline {C.2}IRAF on LODEEN}{30}{subsection.C.2}
\contentsline {subsection}{\numberline {C.3}\textsf {pyraf} installation}{31}{subsection.C.3}
