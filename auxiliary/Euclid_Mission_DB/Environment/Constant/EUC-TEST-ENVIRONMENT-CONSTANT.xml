<?xml version="1.0" encoding="UTF-8"?>
<m:MissionDataBaseSetOfParameters xmlns:m="http://euclid.esa.org/schema/interfaces/sys/mdb">
    <GenericHeader>
        <ProductName>EUC-MDB-ENVIRONMENT-CONSTANT</ProductName>
        <ProductId>1</ProductId>
        <ProductType>MDB</ProductType>
        <SoftwareName>MDBManager</SoftwareName>
        <SoftwareRelease>1.0</SoftwareRelease>
        <ScientificCustodian>SIM</ScientificCustodian>
        <AccessRights>
            <EuclidConsortiumRead>true</EuclidConsortiumRead>
            <EuclidConsortiumWrite>false</EuclidConsortiumWrite>
            <ScientificGroupRead>true</ScientificGroupRead>
            <ScientificGroupWrite>false</ScientificGroupWrite>
        </AccessRights>
    </GenericHeader>
    <EuclidMissionParameterSet>
        <Parameter title="Environment.Constant.BoltzmannConstant" unit="J/K">
            <Description>Boltzmann's constant</Description>
            <Source>Input by J.A.
best-measured value equals 1.3806488E-23(P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants, Web Version 6.2, retrieved on 18 January 2012)</Source>
            <Expression>MolarGasConstant / AvogadroConstant</Expression>
            <Release>0.1</Release>
            <Value>
                <Double>1.3806488e-23</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.AvogadroConstant" unit="1/mol">
            <Description>Avogadro's constant.</Description>
            <Source>J.A.
P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants (Web Version 6.2, retrieved on 18 January 2012).</Source>
            <Expression></Expression>
            <Release>0.1</Release>
            <Value>
                <Double>6.02214129e+23</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.ElectronMass" unit="kg">
            <Description>Electron mass</Description>
            <Source>Input by J.A.
P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants (Web Version 6.2, retrieved on 18 January 2012).</Source>
            <Expression/>
            <Release>0.1</Release>
            <Value>
                <Double>9.10938291e-31</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.BohrRadiusConstant" unit="m">
            <Description>Bohr radius.</Description>
            <Source>J.A.
best-measured value equals 0.52917721092E-10 (P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants, Web Version 6.2, retrieved on 18 January 2012).</Source>
            <Expression></Expression>
            <Release>0.1</Release>
            <Value>
                <Double>5.2917721011e-11</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.VelocityOfLightConstantVacuum" unit="m/s">
            <Description>Velocity of light in vacuum (defining constant)</Description>
            <Source>Field by J.A.:
P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants (Web Version 6.2, retrieved on 18 January 2012). See also the IAU (2009) System of Astronomical Constants (IAU, August 2009, 'IAU 2009 Astronomical Constants', IAU 2009 Resolution B2 adopted at the XXVII-th General Assembly of the IAU. See also IAU, 10 August 2009, 'IAU WG on NSFA Current Best Estimates', http://maia.usno.navy.mil/NSFA/NSFA_cbe.html)</Source>
            <Expression></Expression>
            <Release>0.1</Release>
            <Value>
                <Double>299792458.0</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.AstronomicalUnit2Meter" unit="m">
            <Description>Astronomical unit (au) length. The au is a conventional unit of length and is a defining constant. The numerical value is in agreement with the value adopted in IAU 2009 Resolution B2. The definition applies to all time scales such as TCB, TDB, TCG, TT, etc.</Description>
            <Source>Input by J.A
IAU, August 2012, 'Re-definition of the astronomical unit of length', IAU 2012 Resolution B2 adopted at the XXVIII-th General Assembly of the IAU</Source>
            <Expression></Expression>
            <Release>0.1</Release>
            <Value>
                <Double>149597870700.0</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.ElementaryChargeConstant" unit="C">
            <Description>Elementary charge</Description>
            <Source>Input by J.A.
P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants (Web Version 6.2, retrieved on 18 January 2012).</Source>
            <Expression/>
            <Release>0.1</Release>
            <Value>
                <Double>1.602176565e-19</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.Day2Second" unit="s">
            <Description>Day to second conversion</Description>
            <Source>Input by J.A</Source>
            <Expression/>
            <Release>0.1</Release>
            <Value>
                <Double>86400.0</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.MolarGasConstant" unit="J/mol/K">
            <Description>Molar gas constant</Description>
            <Source>Input by J.A.
P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants (Web Version 6.2, retrieved on 18 January 2012).</Source>
            <Expression/>
            <Release>0.1</Release>
            <Value>
                <Double>8.3144621</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.Degree2Radian" unit="rad">
            <Description>Degree to radian conversion</Description>
            <Source>Input by J.A.</Source>
            <Expression>PiConstant / 180</Expression>
            <Release></Release>
            <Value>
                <Double>1.74532925199433e-2</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.AtomicMassConstant" unit="kg">
            <Description>Atomic mass constant (also known as atomic mass unit [amu]; 1 amu is defined as 1/12-th of the mass of a 12-C atom).</Description>
            <Source>Input by J.A.
Best-measured value equals 1.660538921E-27 (P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants, Web Version 6.2, retrieved on 18 January 2012).</Source>
            <Expression>1.0E-3 / Avogadro_Constant</Expression>
            <Release>0.1</Release>
            <Value>
                <Double>1.660538921e-27</Double>
            </Value>
        </Parameter>
        <Parameter title="Environment.Constant.PlanckConstant" unit="J s">
            <Description>Planck's constant</Description>
            <Source>Field by J.A.:
P.J. Mohr, B.N. Taylor, D.B. Newell, 22 July 2011, 'The 2010 CODATA Recommended Values of the Fundamental Physical Constants', National Institute of Standards and Technology, Gaithersburg, MD 20899-8401; http://www.codata.org/ and http://physics.nist.gov/constants (Web Version 6.2, retrieved on 18 January 2012)</Source>
            <Expression></Expression>
            <Release>0.1</Release>
            <Value>
                <Double>6.62606957e-34</Double>
            </Value>
        </Parameter>
    </EuclidMissionParameterSet>
    <Release>0.1</Release>
    <ReleaseDate>2014-05-04T10:00:00.00Z</ReleaseDate>
    <StartValidity>2014-05-04T10:00:00.00Z</StartValidity>
    <EndValidity>2015-01-04T10:00:00.00Z</EndValidity>
</m:MissionDataBaseSetOfParameters>