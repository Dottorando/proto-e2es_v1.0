'''
 *
 * Copyright (C) 2015 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This script is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 *
 *   Author:
 *
 *   	Erik Romelli
 *   
 *   	PhD Student
 *
 *   	University of Trieste, via Valerio 2, Trieste, Italy
 *   	INAF-OATS, via Bazzoni 2, Trieste, Italy
 *   	room 123
 *   	mail: romelli@oats.inaf.it
 *   	tel: +39 040 3199 123
 *   	cell: +39 340 8991293
 *
 *   Last modified: 06/08/2015
 *   Version: 1.0
 *
'''

# Import standard libraries
from collections import OrderedDict
import sys
import os
import numpy as np
import subprocess
import tempfile
import pyfits
import pickle

# Import dedicated ibraries
from axe_files import FileGenerator
from fits_handler import split_components,create_tips_input_catalog, spec2mono, combine_spectra
from RedshiftExt import redshift_extraction
from csv_table import csv_table
from performance_assessment import PerformanceAssessment

print 'Engine start'

# .log and global settings file names: pay attention editing these variables
log_file = 'SimulatorStatus.log'
overall = 'GlobalParameters.dat'
opt_csv = 'OperationalTimeline_lite.csv'

# delete .log to be sure the one you find is always the new one
os.system('rm %s'%log_file)


# =================== Read the overall configuration ==========================================================

try:  

  print 'Reading global configuration from %s...'%overall
  
  overall_conf = open(overall,'r')
  overall_conf_contents = overall_conf.readlines()
  overall_conf.close()

  global_params = OrderedDict()

  for line in overall_conf_contents:
    line = line.rstrip('\n')
    if line != '':
      key = line.split('=')[0]
      global_params[key] = line.split('=')[1]

  # Open the .log and write that the global configuration has been correctly 
  _log = open(log_file,'w+')
  _log.write('Read configuration file -> Done\n')
  _log.write('\n')
  _log.write('%s content:\n'%overall)
  _log.write('\n')
  _log.write('#####################################\n')
  for line in overall_conf_contents:
    _log.write(line)
  _log.write('#####################################\n')
  _log.write('\n')

except:

  _log = open(log_file,'w+')
  _log.write('Read configuration file -> Failed\n')
  _log.write('Error 404: file %s not found\n'%overall)
  _log.write('\n')
  _log.close()
  print 'An error occured: check the log for further information'
  sys.exit()

# finds the number of sources in the catalog
SOURCES_NUM = pyfits.open('./catalog/%s'%global_params['CATALOG'])[1].header['NAXIS2']

# ============================================================================================================

# =================== Clean directrories and files ===========================================================

if global_params['CleanAll'] == 'T':
  
  print 'Cleaning directories and files from older simulations...'

  _log.write('Cleaninig (All)...\n')
  
  dir_to_be_cleaned = ['./pyraf', \
		       './database', \
		       './output_plot' \
		      ]
  for pointing_id in global_params['pointingID'].split(','):
    dir_to_be_cleaned.append('./pointing%s'%pointing_id)
  
  files_to_be_cleaned = ['./catalog/data_TIPS.fits', \
			 './conf/%s'%global_params['AXE_CONF'], \
			 './conf/tips_configuration_SENS_GRED_realest_A.fits', \
			 './logfile', \
			 './vdm.out', \
			 './Simulation_Statistics.csv' \
			]

  _log.write('Clean directories...\n')
  
  for _dir in dir_to_be_cleaned:    
    os.system('rm -r %s'%_dir)  
    _log.write('%s -> Cleaned\n'%_dir)

  _log.write('All directories cleaned\n')
      
  _log.write('Clean files...\n')

  for _file in files_to_be_cleaned:
    os.system('rm %s'%_file)
    _log.write('%s -> Cleaned\n'%_file)

  _log.write('All files cleaned\n')

  _log.write('Cleaninig complete\n')
  _log.write('\n')

# ============================================================================================================

for point_id in global_params['pointingID'].split(','):

  os.system('mkdir -p ./pointing%s'%point_id)
  os.system('mkdir -p ./pointing%s/%s'%(point_id,global_params['OUTPUT_DIR']))
  _log.write('Create ./pointing%s (if not present)...\n'%point_id)
  _log.write('Create ./pointing%s/%s (if not present)...\n'%(point_id,global_params['OUTPUT_DIR']))
  _log.write('\n')

# =================== Read the ECSS Module ===================================================================

if global_params['Use_Module_ECSS'] == 'T':
  
  print 'Run Euclid_Survey_Strategy_Module...'

  _log.write('Run Euclid_Survey_Strategy_Module...\n')
  
  opt = csv_table(opt_csv)
  #opt_NISP = opt.select(INSTRUMENT = 'NISP', POINTING = 'dither %d'%(int(global_params['dither'])+1))
  
  if len(opt) != 0:
    _log.write('Euclid_Survey_Strategy_Module -> Done\n')
    _log.write('%s correctly loaded\n'%opt_csv)
  else:
    _log.write('Euclid_Survey_Strategy_Module -> Failed\n')
    _log.write('Error 404: file %s not found\n'%opt_csv)
    _log.write('\n')
    print 'An error occured: check the log for further information'
     
else:

  print 'Skip Euclid_Survey_Strategy_Module...'

  _log.write('Euclid_Survey_Strategy_Module -> Skipped\n')
  _log.write('\n')
  
# ============================================================================================================

# =================== Read the SS Module =====================================================================

# OSS: questo modulo dovra' gestirsi ZEUS

if global_params['Use_Module_SS'] == 'T':

  _log.write('Run Simulated_Sky_Module...\n')

  try:
    print 'Run Simulated_Sky_Module...'
    input_catalog = global_params['CATALOG']
    input_catalog_TIPS = input_catalog.split('.')[0]+'_TIPS.fits'
    if global_params['Use_ZEUS'] == 'T':
      #create_tips_input_catalog(input_catalog,input_catalog_TIPS,directory = './catalog/',ZEUS=True)
      print 'WARNING: ZEUS is still not yet implemented! Set Use_ZEUS=F and try again!'
      _log.write('WARNING: ZEUS is still not yet implemented! Set Use_ZEUS=F and try again!')
    else:
      create_tips_input_catalog(input_catalog,input_catalog_TIPS,directory = './catalog/')

    _log.write('Simulated_Sky_Module -> Done\n')
    _log.write('Catalog correctly prepared for TIPS\n')
    _log.write('\n')
  except:
    _log.write('Simulated_Sky_Module -> Failed\n')
    _log.write('Error: check if file names or paths are set properly\n')
    _log.write('\n')    
    print 'An error occured: check the log for further information'

else:

  print 'Skip Simulated_Sky_Module...'

  _log.write('Simulated_Sky_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the SCE Module ====================================================================

# Questo per ora e' un modulo fantasma

if global_params['Use_Module_SCE'] == 'T':

  print 'Run Spacecraft_&_Environment_Module...'

  _log.write('Run Spacecraft_&_Environment_Module...\n')
  
  print 'In this Universe we still have to implement it, try on the other Earth'
  _log.write('Spacecraft_&_Environment_Module -> Done\n')
  _log.write('That was easy, I had nothing to do\n')
  _log.write('\n')
    
else:

  print 'Skip Spacecraft_&_Environment_Module...'

  _log.write('Spacecraft_&_Environment_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the OM Module =====================================================================

# Questo modulo qua dovrebbe aprire il database

if global_params['Use_Module_OM'] == 'T':

  print 'Run Optical_Model_Module...'

  _log.write('Run Optical_Model_Module...\n')
  
  print 'In this Universe we still have to implement it, try on the other Earth'
  _log.write('Optical_Model_Module -> Done\n')
  _log.write('That was easy, I had nothing to do\n')
  _log.write('\n')
    
else:

  print 'Skip Optical_Model_Module...'
  
  _log.write('Optical_Model_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the DS Module =====================================================================

if global_params['Use_Module_DS'] == 'T':

  print 'Run Detection_System_Module...'

  _log.write('Run Detection_System_Module...\n')
  
  for point_id in global_params['pointingID'].split(','):
  
    opt_pointing = opt.select(pointingID = int(point_id))

    _log.write('Pointing %s selected...\n'%point_id)
    _log.write('\n')

    os.system('mkdir -p ./pointing%s/%s'%(point_id,global_params['TIPS_OUTDIR']))
    
    _log.write('Create ./pointing%s/%s (if not present)...\n'%(point_id,global_params['TIPS_OUTDIR']))

    # Prepare a temporary file with a script running the simulation.
    #This is meant to create a separate log with the TIPS output (TIPS is verbose!)
    #SimRun = tempfile.NamedTemporaryFile(dir = './', suffix = '.py')
    SimRunFile = 'tmpSimRun.py'
    SimRun = open(SimRunFile,'w')
    SimRun.write('from SimNISP import NISP_S\n')
    SimRun.write("nisp = NISP_S('%s',inDir = './catalog/',outDir ='./pointing%s/%s')\n"%(input_catalog_TIPS,point_id,global_params['TIPS_OUTDIR']))
    SimRun.write('nisp.get_pointing(%s,%s)\n'%(opt_pointing['ra0'][0],opt_pointing['dec0'][0]))
    SimRun.write("nisp.load_CustomConfig('%s',%s)\n"%(global_params['TIPS_CONFIG'],opt_pointing['exptime'][0]))
    SimRun.write('nisp.run_Simulation()\n')
    SimRun.close()

    # Run the simulation writing a log for the output of TIPS  
    bashString = 'python %s >> ./pointing%s/%s/TIPS_output.log 2>&1'%(SimRunFile,point_id,global_params['TIPS_OUTDIR'])
    os.system(bashString)

    os.system('rm %s'%SimRunFile)

    _log.write('Detection_System_Module_Module -> Done\n')
    _log.write('TIPS correctly run\n')
    _log.write('\n')
  
else:

  print 'Skip Detection_System_Module...'

  _log.write('Detection_System_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the OBDG Module ===================================================================

if global_params['Use_Module_OBDG'] == 'T':

  print 'Run On-Board_Data_Generation_Module...'

  _log.write('Run On-Board_Data_Generation_Module...\n')

  print 'In this Universe we still have to implement it, try on the other Earth'
  _log.write('On-Board_Data_Generation_Module -> Done\n')
  _log.write('That was easy, I had nothing to do\n')
  _log.write('\n')
  
else:

  print 'Skip On-Board_Data_Generation_Module...'

  _log.write('On-Board_Data_Generation_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the DPC Module ====================================================================

if global_params['Use_Module_DPC'] == 'T':
  
  print 'Run Data_Processing_&_Calibration_Module...'

  _log.write('Run Data_Processing_&_Calibration_Module...\n')

  for pointing_id in global_params['pointingID'].split(','):
  
    opt_pointing = opt.select(pointingID = int(pointing_id))

    _log.write('Pointing %s selected...\n'%pointing_id)
    _log.write('\n')
  
    detectors = global_params['detector'].split(',')
    
    for dd in detectors:  

      # I clean the DETECTOR_XX directory to avoid a mess with the files
      os.system('rm -r ./pointing%s/%s/DETECTOR_%s/'%(pointing_id,global_params['OUTPUT_DIR'],dd))
      
      _log.write('Cleaning ./pointing%s/%s/DETECTOR_%s/...\n'%(pointing_id,global_params['OUTPUT_DIR'],dd))

      # I clean the DETECTOR_XX/spectra_analysis directory to avoid a mess with the files
      os.system('rm -r ./pointing%s/%s/DETECTOR_%s/spectra_analysis'%(pointing_id,global_params['OUTPUT_DIR'],dd))
      
      _log.write('Cleaning ./pointing%s/%s/DETECTOR_%s/spectra_analysis...\n'%(pointing_id,global_params['OUTPUT_DIR'],dd))

      # I copy the dispersed images of a single detector in the same directory DETECTOR_XX in which they will be processed
      TIPS_out_list = []

      rootdir = './pointing%s/%s/OUTSIM/'%(pointing_id,global_params['TIPS_OUTDIR'])
      for subdir, dirs, files in os.walk(rootdir):
	for ff in files:
	  TIPS_out_list.append(ff)

      # I create the aXe configuration file  
      FG = FileGenerator(global_params['CATALOG'])

      _log.write('Creating aXe configuration file...\n')

      # I select the SCI files per detector and store them in a separate directory
      to_be_taken=[]
      for ff in TIPS_out_list:
	if ff.find(dd) != -1:
	  to_be_taken.append(ff.split('/')[-1])

      # Create the DETECTOR_XX directory
      os.system('mkdir -p ./pointing%s/%s/DETECTOR_%s/'%(pointing_id,global_params['OUTPUT_DIR'],dd))  

      _log.write('Creating ./pointing%s/%s/DETECTOR_%s/ (if not present)...\n'%(pointing_id,global_params['OUTPUT_DIR'],dd))

      # Create the DETECTOR_XX/spectra_analysis directory
      os.system('mkdir -p ./pointing%s/%s/DETECTOR_%s/spectra_analysis/'%(pointing_id,global_params['OUTPUT_DIR'],dd))  

      _log.write('Creating ./pointing%s/%s/DETECTOR_%s/spectra_analysis (if not present)...\n'%(pointing_id,global_params['OUTPUT_DIR'],dd))

      # Prepare the list of files to combine
    
      #to_combine = [] 

      for ff in to_be_taken:
	os.system('cp ./pointing%s/%s/OUTSIM/%s ./pointing%s/%s/DETECTOR_%s/'%(pointing_id,global_params['TIPS_OUTDIR'],ff,pointing_id,global_params['OUTPUT_DIR'],dd))

	_log.write('cp ./pointing%s/%s/OUTSIM/%s ./pointing%s/%s/DETECTOR_%s/\n'%(pointing_id,global_params['TIPS_OUTDIR'],ff,pointing_id,global_params['OUTPUT_DIR'],dd))

	if ff.find('GRED0') != -1:
	  ditherID = 0
	elif ff.find('GRED90_0_') != -1:
	  ditherID = 1
	elif ff.find('GRED90_1_') != -1:
	  ditherID = 3
	elif ff.find('GRED180') != -1:
	  ditherID = 2
	
	# Create the .dat file for aXe (one for each SCI file    
	dat_name=ff.split('.')[0]+'.dat'
	FG.create_dat(ditherID,opt_pointing['band'][0],ff,dat_name=dat_name,directory='./pointing%s/%s/DETECTOR_%s/'%(pointing_id,global_params['OUTPUT_DIR'],dd))
	FG.create_conf_files(ditherID,global_params['TIPS_CONFIG'],conf_file_name = global_params['AXE_CONF'])

	_log.write('Creating .dat file for ./pointing%s/%s/DETECTOR_%s/%s\n'%(pointing_id,global_params['OUTPUT_DIR'],dd,ff))
    
	# Running aXe
	_log.write('Running aXe on ./pointing%s/%s/DETECTOR_%s/%s\n'%(pointing_id,global_params['OUTPUT_DIR'],dd,ff))
      
	os.system('python ./src/python/Run_aXe.py ./pointing%s/%s/DETECTOR_%s/%s ./pointing%s/%s/DETECTOR_%s/%s ./conf/%s >> ./pointing%s/%s/DETECTOR_%s/%s_aXe.log 2>&1'%(pointing_id, \
																					   global_params['OUTPUT_DIR'], \
																					   dd,\
																					   ff, \
																					   pointing_id, \
																					   global_params['OUTPUT_DIR'], \
																					   dd, \
																					   dat_name, \
																					   global_params['AXE_CONF'], \
																					   pointing_id, \
																					   global_params['OUTPUT_DIR'], \
																					   dd, \
																					   ff.split('.')[0]
																					   )
		)

      # create monodimentional spectra

      source_list = []
      for id_source in range(1,SOURCES_NUM+1):
	source_list.append('BEAM_%dA'%id_source)
    
      _log.write('Converting extracted spectra in monodimensional...\n')

      for source in source_list:
      
	to_combine = []

	for ff in to_be_taken:
	
	  spec_file = ff.split('.')[0]+'_2.SPC.fits'
	
	  try:
	    spec2mono(spec_file,source,directory='./pointing%s/%s/DETECTOR_%s/'%(pointing_id,global_params['OUTPUT_DIR'],dd))

	    to_combine.append('./pointing%s/%s/DETECTOR_%s/%s_2_MONO.fits'%(pointing_id,global_params['OUTPUT_DIR'],dd,ff.split('.')[0]))
	
	    print 'Spectrum %s found in ./pointing%s/%s/DETECTOR_%s/%s...'%(source,pointing_id,global_params['OUTPUT_DIR'],dd,spec_file)

	    _log.write('Spectrum %s found in ./pointing%s/%s/DETECTOR_%s/%s...\n'%(source,pointing_id,global_params['OUTPUT_DIR'],dd,spec_file))

	  except:
	    pass
      
	if to_combine != []:
	
	  _log.write('Combining spectra...\n')

	  combine_spectra(to_combine, directory = './pointing%s/%s/DETECTOR_%s/'%(pointing_id,global_params['OUTPUT_DIR'],dd))
	  
	  # Extracting z

	  # Delete previuos idFILE (IRAF stuff) and input files (I had to copy that files in the main directory because of IRAF)
	  _log.write('Cleaning IRAF environment...\n')

	  os.system('rm -r ./pyraf; rm -r ./database')

	  _log.write('Extracting z...\n')

	  # IRAF is not very smart so I have to run it in the proper directory
	  # Is easyer to copy the composed spectrum in the main directory
	  # This is one of the things to solve in further versions
	  os.system('cp ./pointing%s/%s/DETECTOR_%s/Combined_Spectrum.fits .'%(pointing_id,global_params['OUTPUT_DIR'],dd))

	  # HO TORLTO UN ATTIMO IL TRY PER CONTROLLARE: RICORDA DI RIMETTERLO

	  try:
	    zExt=redshift_extraction('Combined_Spectrum.fits')
	    zExt.ideIRAF()
	    zExt.Continuum()
	    zExt.Splot()

	    if global_params['z_delta_lambda']=='None':
	      zExt.RedshiftMeasure()
	    else:
	      try:
		zExt.RedshiftMeasure(delta_lambda = float(global_params['z_delta_lambda']))
	      except:
		zExt.RedshiftMeasure()
		print 'z_delta_lambda=%s was never an option. Allowed flags: None or a number. I used None to finish the job'%global_params['z_delta_lambda']
		_log.write('z_delta_lambda=%s was never an option. Allowed flags: None or a number. I used None to finish the job\n'%global_params['z_delta_lambda'])

	    pickle.dump(zExt,open('./pointing%s/%s/DETECTOR_%s/spectra_analysis/%s.p'%(pointing_id,global_params['OUTPUT_DIR'],dd,source),'wb'))

	    # Clean combined spectra (both main and DETECTOR_XX before the next source to avoid spourius effect
	    os.system('rm Combined_Spectrum.fits; rm ./pointing%s/%s/DETECTOR_%s/Combined_Spectrum.fits; rm Combined_Spectrum_CONT.fits'%(pointing_id,global_params['OUTPUT_DIR'],dd))
	    
	  except:
	    print 'There is something strange in %s neighbourhood!'%source
	    
	    _log.write('IRAF encountered some problems in source %s and it was jumped...\n'%source)

    
    _log.write('Data_Processing_&_Calibration_Module -> Done\n')
    _log.write('\n')
    _log.write('\n')
  
else:

  print 'Skip Data_Processing_&_Calibration_Module...'

  _log.write('Data_Processing_&_Calibration_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

# =================== Read the PA Module ===================================================================

if global_params['Use_Module_PA'] == 'T':

  print 'Run Performance_Assessment_Module...'

  _log.write('Run Performance_Assessment_Module...\n')

  # Create the PA Module directory in witch the simulator stores the output
  os.system('mkdir -p ./output_plot')

  _log.write('Creating ./output_plot (if not present)...\n')

  PA=PerformanceAssessment(map(int, global_params['pointingID'].split(',')))
  PA.load_sources()
  
  PA.plot_z_per_pointing(directory = './output_plot/')
  PA.plot_hit('data_TIPS.fits', directory = './output_plot/')
  PA.plot_residuals(directory = './output_plot/')
  PA.plot_matches(directory = './output_plot/')

  print 'Plotting...'
  _log.write('Plotting...\n')

  PA.evaluate_statistics('Simulation_Statistics.csv')
 
  print 'Evaluating statistics...'
  _log.write('Evaluating statistics...\n')
    
  stats = csv_table('./Simulation_Statistics.csv')
  
  _log.write('Simulation statistics:\n')  
  _log.write('#####################################\n')
  print 'Simulation statistics:'
  print ''
  for k in stats.keys():
    print k, stats[k][0]
    _log.write('%s: %s\n'%(k,stats[k][0]))
  _log.write('#####################################\n')
  
  _log.write('Performance_Assessment_Module -> Done\n')
  _log.write('\n')
  
else:

  print 'Skip Performance_Assessment_Module...'

  _log.write('Performance_Assessment_Module -> Skipped\n')
  _log.write('\n')

# ============================================================================================================

print ''
print "And that's all folks!"
_log.write("And that's all folks!\n")
_log.write('\n')
_log.close()

