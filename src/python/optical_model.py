class OpticalModel:
  
  def __init__(self, conf_file, directory = './'):
    
    import pyfits
    
    self.directory = directory
    self.conf_file = conf_file
    self.NISP_db_root = './auxiliary/Euclid_Mission_DB/SpaceSegment/Instrument/NISP/'
    
    self.conf = pyfits.open(self.directory+self.conf_file, mode = 'update')
  
  def load_NISP_as_designed(self):
    
    from Mdb import Mdb
    
    self.DB = Mdb(self.NISP_db_root+'NISPAsDesigned/'+'EUC-TEST-SPACESEGMENT-INSTRUMENT-NISP-NISPASDESIGNED.xml')
  
  def load_NISP_as_required(self):
    
    from Mdb import Mdb
    
    self.DB = Mdb(self.NISP_db_root+'NISPAsRequired/'+'EUC-TEST-SPACESEGMENT-INSTRUMENT-NISP-NISPASREQUIRED.xml')

  def load_NISP_as_simulated(self):
    
    print 'This section does not exist! Try in a near future or take your T.A.R.D.I.S.'

  def load_NISP_current_best_estimate(self):
    
    from Mdb import Mdb
    
    self.DB = Mdb(self.NISP_db_root+'NISPCurrentBestEstimate/'+'EUC-TEST-SPACESEGMENT-INSTRUMENT-NISP-NISPCURRENTBESTESTIMATE.xml')
    
  def change_parameter(self, table, param_key, DB_path_to_param):
        
    self.add_parameter(table, param_key, DB_path_to_param)
  
  def add_parameter(self, table, param_key, DB_path_to_param):
    
    self.conf[table].header[param_key] = self.DB.getValue(DB_path_to_param)
    self.conf.flush()
  
  def del_parameter(self, table, param_key):

    self.conf[table].header.pop(param_key, None)
    self.conf.flush()
  
  def create_new_config_file(self, file_name, directory = './'):

    import pyfits

    pass
