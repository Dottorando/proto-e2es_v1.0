#THIS FUNCTION IS MEANT TO LET dict2fits RUN PROPERLY

def dict2fits_metadata(Dict) :
   "Try to compile a description from the dictionary"
   import pyfits
   import numpy as np
   lstn=Dict.keys()
   description = {}
   description['name']=[]
   description['format']=[]
   description['comment']=[]
   description['unit']=[]
   fmtT={'bool':'L','int16':'I','int32':'J','int64':'K','string':'A','float64':'D','complex128':'M'}
   count=0
   for ttype in lstn :
      if type(Dict[ttype]) == type({}) :
         print "%s is a dictionary, skipped"
      else :
         try :
            fmt = fmtT[Dict[ttype].dtype.name]
         except :
            if np.array(Dict[ttype]).dtype.name[0:3]=='str':
               fmt='A'
            else :
               print "Format %s not found"%np.array(Dict[ttype]).dtype.name
               fmt=''
         if fmt!='' :
            size=''
            if fmt == 'A':
               #dt = np.array(Dict[ttype][0]).dtype
               ll=1
               for k in Dict[ttype] :
                  if ll < len(k) :
                     ll = len(k)
               size='%d'%ll
            else :
               shape = Dict[ttype].shape
               if len(shape) > 1 :
                  size='%d'%shape[1]
            fmt=size+fmt
            description['name'].append(ttype)
            description['format'].append(fmt)
            description['unit'].append('')
            description['comment'].append('')
            count+=1
   description['number']=count
   return description

def split_components(file_name,component=None,directory = './'):

  import pyfits
  import numpy as np	

  ff = pyfits.open(directory+file_name)
  if component == None:
    for cont in np.arange(1,len(ff)):
      hdulist=[]
      hdulist.append(pyfits.PrimaryHDU())
      hdulist.append(ff[cont])
      thdulist=pyfits.HDUList(hdulist)
      new_file_name = file_name.split('.')[0]+'_'+ff[cont].header['EXTNAME']+'.fits'
      thdulist.writeto(directory+new_file_name,clobber=True)
  else:
      hdulist=[]
      hdulist.append(pyfits.PrimaryHDU())
      hdulist.append(ff[component])
      thdulist=pyfits.HDUList(hdulist)
      new_file_name = file_name.split('.')[0]+'_'+ff[component].header['EXTNAME']+'.fits'
      thdulist.writeto(directory+new_file_name,clobber=True)
    

def fits2dict(file_name,table,directory = './'):

  import pyfits
  from collections import OrderedDict
  
  ff = pyfits.open(directory+file_name)
  scidata = ff[table].data
  key_list = scidata.names
  scidata_dict = OrderedDict()
  for k in key_list:
    scidata_dict[k]=scidata.field(k)
  return scidata_dict

def dict2fits(Dict,file_name,directory = './'):

   import pyfits
   import numpy as np

   _description=dict2fits_metadata(Dict)
   c=[]
   for kid in range(len(_description['name'])):
     name = _description['name'][kid]
     c.append(pyfits.Column(array=Dict[name],name=name,format=_description['format'][kid]))
   coldefs=pyfits.ColDefs(c)
   if pyfits.__version__ == '3.2.4':
    tbhdu=pyfits.new_table(coldefs)
   else:
    tbhdu=pyfits.BinTableHDU.from_columns(coldefs)
   tbhdu.writeto(directory+file_name,clobber = True)

def spec2mono(file_name,table,directory = './'):

  import pyfits
  import numpy as np

  ff = pyfits.open(directory+file_name)
  rows=ff[table].header['naxis2']
  scidata=ff[table].data
  lambdastart=scidata[0][2]
  lambdaend=scidata[rows-1][2]
  deltalambda=(lambdaend-lambdastart)/(rows-1)
  flux=np.zeros((1,rows))
  for row in range(rows):
    flux[0,row]=scidata[row][9]
  new_hdu=pyfits.PrimaryHDU(flux)
  new_hdu.header['crval1']=lambdastart
  new_hdu.header['cdelt1']=deltalambda
  new_hdu.header['ctype1']='LAMBDA'
  fits_mono=pyfits.HDUList([new_hdu])
  new_file_name = file_name.split('.')[0]+'_MONO'+'.fits'
  fits_mono.writeto(directory+new_file_name,clobber=True)
  
def combine_spectra(spectra_list,combined_name = 'Combined_Spectrum.fits',directory = './'):
  
  import pyfits
  import numpy as np
  from collections import OrderedDict
  from copy import deepcopy
  
  spectra = []
  for spec in spectra_list:
    ff=pyfits.open(spec)
    spectra.append((spec,ff[0].data[0]))
  try:
    sum_spec = deepcopy(spectra[0][1])
    for i_spec in range(1,len(spectra)):
      sum_spec+=spectra[i_spec][1]
  except:
    spectra.sort(key = lambda s: len(s[1]), reverse=True)
    sum_spec = deepcopy(spectra[0][1])
    for i_spec in range(1,len(spectra)):
      f1,l1=mono2LambdaFlux(spectra[0][0])
      f2,l2=mono2LambdaFlux(spectra[i_spec][0])
      finterp=np.interp(l1[0],l2[0],f2[0])
      sum_spec+=finterp
  sum_spec = sum_spec/float(len(spectra_list))
  flux_sum=np.zeros((1,len(sum_spec)))
  for row in range(len(sum_spec)):
    flux_sum[0,row]=sum_spec[row]
  new_hdu=pyfits.PrimaryHDU(flux_sum)
  new_hdu.header['crval1']=pyfits.open(spectra_list[0])[0].header['crval1']
  new_hdu.header['cdelt1']=pyfits.open(spectra_list[0])[0].header['cdelt1']
  new_hdu.header['ctype1']='LAMBDA'
  fits_sum=pyfits.HDUList([new_hdu])
  fits_sum.writeto(directory+combined_name,clobber=True)
  

def mono2LambdaFlux(file_name,directory = './'):

  import pyfits
  import numpy as np

  f = pyfits.open(directory+file_name)
  rows = f[0].header['NAXIS1']
  lstart = f[0].header['CRVAL1']
  ldelta = f[0].header['CDELT1']
  lend = ldelta*(rows-1)+lstart
  _lambda = np.zeros((1,rows))
  _lambda[0,0] = lstart
  for row in range(1,rows):
    _lambda[0,row]=_lambda[0,row-1]+ldelta
  _flux = f[0].data
  return _flux,_lambda
  
def create_tips_input_catalog(fits_file_in,fits_file_out,amin=-999.,amax=999.,dmin=-999.,dmax=999.,minmag=-999.,maxmag=999.,zmin=-999.,zmax=999.,fmin=-999.,fmax=999.,directory = './',ZEUS = False):
  
  from catalog_handler import CatalogHandler
  from collections import OrderedDict
  import numpy as np
  #from dict2fits import Table
  import pyfits
  
  CH = CatalogHandler(fits_file_in, directory = directory,ZEUS = ZEUS)
  # select coordinates and magnitudes range    
  CH.coordRange(amin,amax,dmin,dmax)    
  CH.magRange(minmag,maxmag)
  CH.zRange(zmin,zmax)
  CH.fluxRange(fmin,fmax)
  #CONCATENA LE LISTE DI INDICI E PRENDE L'INTERSEZIONE
  CH.allRange()

  tips_catalog = OrderedDict()

  tips_catalog['NUMBER']=[]
  tips_catalog['MODSPEC']=[]
  tips_catalog['MODIMG']=[]
  tips_catalog['RA'] = []
  tips_catalog['DEC']=[]
  tips_catalog['A_SKY']=[]
  tips_catalog['B_SKY']=[]
  tips_catalog['THETA_SKY']=[]
  tips_catalog['MODFILE']=[]
  tips_catalog['IMGFILE']=[]
  ids = CH.catalog_dict['num'][CH.indexes]
  for _id in ids:
    data = CH.get_object(_id)
    tips_catalog['NUMBER'].append(_id)
    tips_catalog['MODSPEC'].append(1)
    if data['bt']>=0:
      tips_catalog['MODIMG'].append(0)
    else:
      tips_catalog['MODIMG'].append(-1)	# point-like object
    tips_catalog['RA'].append(data['alpha'])
    tips_catalog['DEC'].append(data['delta'])
    tips_catalog['A_SKY'].append(1.0) # unused by TIPS
    tips_catalog['B_SKY'].append(1.0) # unused by TIPS                        
    tips_catalog['THETA_SKY'].append(0.0)
    tips_catalog['MODFILE'].append(data['incident']) 
    tips_catalog['IMGFILE'].append(data['image'])   # FIXME Data object does not use the rotation; TIPS will process the rotation internally (currently under development)
  #CONVERT ALL THE LISTS IN THE DICTIONARY IN ARRAY
  tips_catalog['NUMBER']=np.array(tips_catalog['NUMBER'])
  tips_catalog['MODSPEC']=np.array(tips_catalog['MODSPEC'])
  tips_catalog['MODIMG']=np.array(tips_catalog['MODIMG'])
  tips_catalog['RA'] = np.array(tips_catalog['RA'])
  tips_catalog['DEC']=np.array(tips_catalog['DEC'])
  tips_catalog['A_SKY']=np.array(tips_catalog['A_SKY'])
  tips_catalog['B_SKY']=np.array(tips_catalog['B_SKY'])
  tips_catalog['THETA_SKY']=np.array(tips_catalog['THETA_SKY'])
  tips_catalog['MODFILE']=np.array(tips_catalog['MODFILE'])
  tips_catalog['IMGFILE']=np.array(tips_catalog['IMGFILE'])
  
  dict2fits(tips_catalog,fits_file_out,directory=directory)
  
  
