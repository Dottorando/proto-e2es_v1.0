__DESCRIPTION__="""

Author: M.Maris
Version: 1.1
Issue: 2014 Dec 10 - 2015 Feb 1 - 2015 Jul 11 -

Libray used to handle a corot map

CorotMap is defined with two transforms

1 .Ecliptic -> Unflipped map
   ecliptic logitude rotation 
2 .Unflipped map -> Map
   -X->Z, Y->Y, Z->X
   
   
- 2015 Jul 11 -
Fixed a bug in Coord_Ecl2Corot
  line with wrogth statement
      phi=np.arctan2(c[0],c[1])
  replaced with correct one
      phi=np.arctan2(c[1],c[0])
the bug caused Coord_Ecl2Corot to inrpret latitude as colatitude.

Added in fits2corot_mean fuction
  mean_corot_map_at_epoch(day)
which allows to create a corot map without the inclusion of the seasonal dependence.

P. Battaglia - Defined the new function Coord_Eq2Corot which for given equatorial coordinates returns corresponding corot coords
eq_RA,eq_DEC in deg 
theta, phi in radians
"""
class corot_map :
   """corot_map object handles a corotating map for a given longitude"""
   def __init__(self,nside,solar_longitude_deg,Map=None) :
      """needs the Nside of the map, the solar longitude"""
      import numpy as np
      if nside == None : return
      self.nside=nside
      self.solar_longitude_deg=solar_longitude_deg
      self.CSE=np.cos(np.deg2rad(self.solar_longitude_deg))
      self.SSE=np.sin(np.deg2rad(self.solar_longitude_deg))
      self.map=Map
   def Pnt_UnflippedCorot2Map(self,PntUnflippedCorot) :
      "From Map r.f. returns corot r.f."
      import numpy as np
      return np.array([-PntUnflippedCorot[2],PntUnflippedCorot[1],PntUnflippedCorot[0]])
   def Pnt_Map2UnflippedCorot(self,PntMap) :
      "From Flip r.f. returns map r.f."
      import numpy as np
      return np.array([PntMap[2],PntMap[1],-PntMap[0]])
   def Pnt_Ecl2UnflippedCorot(self,PntEcl) :
      "Rotates Ecliptic to Unflipped"
      import numpy as np
      return np.array([self.CSE*PntEcl[0]+self.SSE*PntEcl[1],-self.SSE*PntEcl[0]+self.CSE*PntEcl[1],PntEcl[2]*1])
   def Pnt_UnflippedCorot2Ecl(self,PntUnflippedCorot) :
      "Rotates Unflipped to Ecliptic"
      import numpy as np
      return np.array([self.CSE*PntUnflippedCorot[0]-self.SSE*PntUnflippedCorot[1]
	    ,self.SSE*PntUnflippedCorot[0]+self.CSE*PntUnflippedCorot[1]
	    ,PntUnflippedCorot[2]*1])
   def Pnt_Ecl2Side(self,*arg) :
      """Generates as many pointings as manny pixels in the map and 
	 returns the corresponding solar aspect angle.
      """
      import healpy as H
      import numpy as np
      import copy
      if len(arg)== 0:
         idxEcl=np.arange(12*self.nside**2)
         pntEcl=np.array(H.pix2vec(int(self.nside),idxEcl))
      else :
         pntEcl=np.array(arg[0])
      Xcorot=self.CSE*pntEcl[0]+self.SSE*pntEcl[1]
      Ycorot=-self.SSE*pntEcl[0]+self.CSE*pntEcl[1]
      return np.rad2deg(np.arctan2(Xcorot,Ycorot))
   def Pnt_Ecl2Corot(self,*arg) :
      """returns the coordinates of pixels in corotating map
         .Pnt_Ecl2Corot() 
            returns a two elements tuple
               pntMap for all the pixels of a map
               idxMap for all the pixels of a map
               idxEcl for all the pixels of a map
         .Pnt_Ecl2Corot(pntEcl) 
            pntEcl = [array(x),array(y),array(z)]
            returns a two elements tuple
               pntMap for all the pixels of a map
               idxMap for all the pixels of a map
      """
      import healpy as H
      import numpy as np
      import copy
      if len(arg)== 0:
         idxEcl=np.arange(12*self.nside**2)
         pntEcl=np.array(H.pix2vec(int(self.nside),idxEcl))
      else :
         pntEcl=np.array(arg[0])
      pnt1=pntEcl.copy()
      pnt1[0]=self.CSE*pntEcl[0]+self.SSE*pntEcl[1]
      pnt1[1]=-self.SSE*pntEcl[0]+self.CSE*pntEcl[1]
      pnt1[2]=pntEcl[2]*1
      pntMap=pnt1.copy()
      pntMap[0]=-pnt1[2]*1
      pntMap[1]=pnt1[1]*1
      pntMap[2]=pnt1[0]*1
      idxMap=H.vec2pix(int(self.nside),pntMap[0],pntMap[1],pntMap[2])
      if len(arg)== 0:
         return pntMap,idxMap,idxEcl
      else :
         return pntMap,idxMap
   def Coord_Ecl2Corot(self,ecl_lon,ecl_lat) :
      """for given ecliptic coordinates returns corresponding corot coords
         ecl_long,ecl_lat in deg
         theta, phi in radiatns
      """
      import healpy as H
      import numpy as np
      import copy
      elor=np.deg2rad(ecl_lon)
      elar=np.deg2rad(ecl_lat)
      x=np.cos(elor)*np.cos(elar)
      y=np.sin(elor)*np.cos(elar)
      z=np.sin(elar)
      p=np.array([x,y,z])
      side=self.Pnt_Ecl2Side(p)
      c,i=self.Pnt_Ecl2Corot(p)
      po=(c[0]**2+c[1]**2)**0.5
      phi=np.arctan2(c[1],c[0])
      theta=np.arctan2(po,c[2])
      return theta,phi,side

   def Coord_Eq2Corot(self, eq_RA, eq_DEC):
      """for given equatorial coordinates returns corresponding corot coords
         eq_RA,eq_DEC in deg 
         theta, phi in radians
      """
      import healpy as H
      import numpy as np
      import copy
      RA_rad=np.deg2rad(eq_RA)
      DEC_rad=np.deg2rad(eq_DEC)
      x=np.cos(RA_rad)*np.cos(DEC_rad)
      y=np.sin(RA_rad)*np.cos(DEC_rad)
      z=np.sin(DEC_rad)
      epsilon=np.deg2rad(23.439) #Obliquita' Eclittica J2000.0
      xecl=x
      yecl=np.cos(epsilon)*y+np.sin(epsilon)*z
      zecl=-np.sin(epsilon)*y+np.cos(epsilon)*z
      p=np.array([xecl,yecl,zecl])
      side=self.Pnt_Ecl2Side(p)
      c,i=self.Pnt_Ecl2Corot(p)
      po=(c[0]**2+c[1]**2)**0.5
      phi=np.arctan2(c[1],c[0])
      theta=np.arctan2(po,c[2])
      return theta,phi,side

   def Pnt_Corot2Ecl(self,*arg) :
      """returns the coordinates of pixels from corotating to ecliptical map
         .Pnt_Corot2Ecl() 
            returns a two elements tuple
               pntEcl for all the pixels of a map
               idxEcl for all the pixels of a map
               idxEMap for all the pixels of a map
         .Pnt_Corot2Ecl(pntMap) 
            pntMap = [array(x),array(y),array(z)]
            returns a two elements tuple
               pntEcl for all the pixels of a map
               idxEcl for all the pixels of a map
      """
      import healpy as H
      import numpy as np
      import copy
      if len(arg)== 0:
         idxMap=np.arange(12*self.nside**2)
         pntMap=np.array(H.pix2vec(int(self.nside),idxMap))
      else :
         pntMap=np.array(arg[0])
      pnt1=pntMap.copy()
      pnt1[0]=pntMap[2]*1
      pnt1[1]=pntMap[1]*1
      pnt1[2]=-pntMap[0]
      pntEcl=pntMap.copy()
      pntEcl[0]=self.CSE*pnt1[0] -self.SSE*pnt1[1]
      pntEcl[1]=self.SSE*pnt1[0] +self.CSE*pnt1[1]
      pntEcl[2]=pnt1[2]*1
      idxEcl=H.vec2pix(int(self.nside),pntEcl[0],pntEcl[1],pntEcl[2])
      if len(arg)== 0:
         return pntEcl,idxEcl,idxMap
      else :
         return pntEcl,idxEcl
   def map2ecl(self) :
      """returns a map (if present) with pixels in ecliptic coordinates.
      No interpolation is performed on pixels
      """
      if self.map == None : return
      v=self.Pnt_Ecl2Corot()[1]
      return self.map[v]
   def resampleMap(self,theta,phi) :
      """resample a map , interpolation is performed on pixels
      """
      import healpy as H
      import numpy as np
      return H.pixelfunc.get_interp_val(self.map,theta,phi)
   def resampleMapEcliptic(self,ecl_lon,ecl_lat) :
      """resampleMap at given ecliptic coordinates"""
      import healpy as H
      import numpy as np
      theta,phi,side=self.Coord_Ecl2Corot(ecl_lon,ecl_lat)
      return self.resampleMap(theta,phi),side
   def resampleMapEquatorial(self,eq_RA,eq_DEC) :
      """resampleMap at given equatorial coordinates"""
      import healpy as H
      import numpy as np
      theta,phi,side=self.Coord_Eq2Corot(eq_RA,eq_DEC)
      return self.resampleMap(theta,phi),side

class fits2corot_day(corot_map) :
   """convert a fits corot_map to a dayly map"""
   def __init__(self,fname,offsetipix=0) :
      import numpy as np
      import pyfits
      corot_map.__init__(self,None,None,Map=None)
      self.fname=fname
      self.p=pyfits.open(self.fname)
      self.hdr=self.p[1].header.copy()
      self.day=self.hdr['day']
      self.solar_longitude_deg=self.p[1].header['solar_long']
      self.nside=self.p[1].header['nside']
      self.CSE=np.cos(np.deg2rad(self.solar_longitude_deg))
      self.SSE=np.sin(np.deg2rad(self.solar_longitude_deg))
      str(self.p[1].header)
      self.map=self.serialize(self.p[1].data['z'])
   def column2map(self,name) :
      return self.serialize(self.p[1].data[name])
   def serialize(self,x):
      import numpy as np
      y=np.array(x)
      y.shape=y.size
      return y

class fits2corot_mean :
   """convert a fits corot_map to a fourier transform"""
   class __fourier :
      def __init__(self,p) :
         import numpy as np
         self.t=p[1].data['DAY']*1.
         self.fourier_max_order=p[2].header['FOURIER_MAX_ORDER']
         self.fourier_base_norm=p[2].header['FOURIER_BASE_NORM']
         self.c=np.zeros([self.fourier_max_order+1,len(self.t)])
         self.s=np.zeros([self.fourier_max_order+1,len(self.t)])
         self.period=365.
         for k in range(self.fourier_max_order+1) :
            self.c[k]=np.cos(k*2*np.pi/self.period*self.t)*(self.fourier_base_norm if k>0 else 1.)
            self.s[k]=np.sin(k*2*np.pi/self.period*self.t)*(self.fourier_base_norm if k>0 else 1.)
      def get_coeff(self,p,ipix) :
         import numpy as np
         self.zc=np.zeros(self.fourier_max_order+1)
         self.zs=np.zeros(self.fourier_max_order+1)
         self.zc[0]=p[2].data['Z_MEAN'][ipix]
         for k in range(1,self.fourier_max_order+1) :
            self.zc[k] = p[2].data['Z_COS'+str(k)][ipix]
            self.zs[k] = p[2].data['Z_SIN'+str(k)][ipix]
      def value(self,p,ipix,maxorder=None) :
         import numpy as np
         self.get_coeff(p,ipix)
         v=self.t*0.
         for k in range(len(self.t)) : 
            p1=self.c[:,k]*self.zc
            p2=self.s[:,k]*self.zs
            i1=(maxorder+1) if (maxorder != None) else self.fourier_max_order
            v[k]=p1[0:i1].sum()+p2[0:i1].sum()
         return v
      def map(self,p,day) :
         import numpy as np
         v=np.zeros(p[2].header['npix'])
         for k in range(1,self.fourier_max_order+1) : 
            v+=np.cos(k*2.*np.pi/self.period*day)*p[2].data['Z_COS'+str(k)]
            v+=np.sin(k*2.*np.pi/self.period*day)*p[2].data['Z_SIN'+str(k)]
         return v*self.fourier_base_norm+p[2].data['Z_MEAN']
      def z_mean(self,p) :
         """return data['Z_MEAN']"""
         return p[2].data['Z_MEAN']
   def __init__(self,fname,offsetipix=0) :
      import pyfits
      self.fname=fname
      self.p=pyfits.open(self.fname)
      self.hdr=self.p[1].header.copy()
      self.offsetipix=offsetipix
      self.ft=self.__fourier(self.p)
      self.days=self.p[1].data['day']
      self.solar_longitude_deg=self.p[1].data['solar_long']
      self.nside=self.p[2].header['nside']
   def tl_ft(self,ipix) :
      return self.ft.value(self.p,ipix)
   def selectipix(self,ipix) :
      import numpy as np
      return np.where(self.p[3].data['ipix']-self.offsetipix==ipix)[0] 
   def tl_list_ipix(self) :
      import numpy as np
      return np.unique(self.p[3].data['ipix'])-self.offsetipix
   def tl_get(self,ipix) :
      import numpy as np
      idx=self.selectipix(ipix)
      return self.p[3].data['Izody'][idx]
   def tl_view(self,ipix) :
      import numpy as np
      idx=self.selectipix(ipix)
      plot(self.p[3].data['Izody'][idx],label=str(ipix)) 
      print self.p[3].data['Izody'][idx].mean(), self.p[2].data['z_mean'][ipix] , rad2deg(self.p[3].data['map_colat'][idx[0]]),rad2deg(self.p[3].data['map_long'][idx[0]]),self.p[3].data['Izody'][idx].ptp()
   def corot_map_at_epoch(self,day) :
      """returns a zle corot map at given epoch (day of year) """
      import numpy as np
      sl=np.interp(np.mod(day,365.),self.days,self.solar_longitude_deg)
      return corot_map(self.nside,sl,Map=self.ft.map(self.p,day))
   def mean_corot_map_at_epoch(self,day) :
      """returns the time averaged zle corot map at given epoch (day of year) """
      import numpy as np
      sl=np.interp(np.mod(day,365.),self.days,self.solar_longitude_deg)
      return corot_map(self.nside,sl,Map=self.ft.z_mean(self.p))

if __name__=='__main__' :
   import sys
   import healpy as H
   import numpy as np
   print "Test ",sys.argv[0]
   
   # test ecliptic to map v.z. map to ecliptic 1 step
   # from a set of Elcliptic coordinates 
   # for each longitude
   #   computes map coordinates using the one shot function
   #   reverses to ecliptic coordinates using the one shot function
   #   computes the divergence = 1-dot_product the input and output ecliptic
   #     dot_product should be 0.
   print "\nTest ecliptic to map v.z. map to ecliptic 1 step"
   nside=16
   ipixEcl0=np.arange(12*nside**2)
   PEcliptic0=H.pix2vec(nside,ipixEcl0)
   print "lon worst divergence"
   for elong in arange(0,360+15,15):
      cm_16_0=corot_map(16,elong)
      PMap=cm_16_0.Pnt_Ecl2Corot(PEcliptic0)
      PEclipticFromPMap=cm_16_0.Pnt_Corot2Ecl(PMap[0])
      divergence=1-(PEclipticFromPMap[0]*PEcliptic0).sum(axis=0)
      print "%3.0f %5.0e"%(elong,max(abs(divergence)))
      
   # test map to ecliptic v.z. ecliptic to map 1 step
   # from a set of Map coordinates 
   # for each longitude
   #   computes ecliptic coordinates using the one shot function
   #   reverses to map coordinates using the one shot function
   #   computes the divergence = 1-dot_product the input and output map
   #     dot_product should be 0.
   print "\nTest map to ecliptic v.z. ecliptic to map 1 step"
   nside=16
   ipixMap0=np.arange(12*nside**2)
   PMap0=H.pix2vec(nside,ipixMap0)
   print "lon worst divergence"
   for elong in arange(0,360+15,15):
      cm_16_0=corot_map(16,elong)
      PEcliptic=cm_16_0.Pnt_Corot2Ecl(PMap0)
      PMapFromPEcliptic=cm_16_0.Pnt_Ecl2Corot(PEcliptic[0])
      divergence=1-(PMapFromPEcliptic[0]*PMap0).sum(axis=0)
      print "%3.0f %5.0e"%(elong,max(abs(divergence)))
   
   # test from ecliptic to map single and two steps
   # from a set of Ecliptic coordinates 
   # for each longitude
   #   computes corot map coordinates using the one shot function
   #   computes corot map coordinates using the two steps procedure
   #   computes the divergence = 1-dot_product between the two vectors
   #     dot_product should be 0.
   print "\nTest from ecliptic to map single and two steps"
   nside=16
   ipixEcl0=np.arange(12*nside**2)
   PEcliptic0=H.pix2vec(nside,ipixEcl0)
   print "lon worst divergence"
   for elong in arange(0,360+15,15):
      cm_16_0=corot_map(16,elong)
      PCorot=cm_16_0.Pnt_Ecl2Corot()
      PUnflippedCorotFromEcliptic=cm_16_0.Pnt_Ecl2UnflippedCorot(PEcliptic0)
      PMapFromEcliptic=cm_16_0.Pnt_UnflippedCorot2Map(PUnflippedCorotFromEcliptic)
      divergence=1-(PCorot[0]*PMapFromEcliptic).sum(axis=0)
      print "%3.0f %5.0e"%(elong,max(abs(divergence)))
      
   # test from map to ecliptic single and two steps
   # from a set of Map coordinates 
   # for each longitude
   #   computes ecliptic coordinates using the one shot function
   #   computes ecliptic coordinates using the two steps procedure
   #   computes the divergence = 1-dot_product between the two vectors
   #     dot_product should be 0.
   print "Test from map to ecliptic single and two steps"
   nside=16
   ipixMap0=np.arange(12*nside**2)
   PMap0=H.pix2vec(nside,ipixMap0)
   print "lon worst divergence"
   for elong in arange(0,360+15,15):
      cm_16_0=corot_map(16,elong)
      PEcliptic=cm_16_0.Pnt_Corot2Ecl()
      PUnflippedCorotFromMap=cm_16_0.Pnt_Map2UnflippedCorot(PMap0)
      PEclipticFromMap=cm_16_0.Pnt_UnflippedCorot2Ecl(PUnflippedCorotFromMap)
      divergence=1-(PEcliptic[0]*PEclipticFromMap).sum(axis=0)
      print "%3.0f %5.0e"%(elong,max(abs(divergence)))
   
   
