class PerformanceAssessment:
  
  def __init__(self,pointingID_list,detlist = ['00','01','02','03','10','11','12','13','20','21','22','23','30','31','32','33']):
    
    import numpy as np
    
    self.pointingID_list = pointingID_list
    self.sigma2fwhm = 2*np.sqrt(2*np.log(2.))
    self.reference_z = 1.32370328903198 #HARDCODED
    self.detlist = detlist

  def load_sources(self, parent_directory = './'):
    
    import os
    import pickle
    import pyfits
    
    cat=pyfits.open(parent_directory+'catalog/data_TIPS.fits')
    self.N=int(cat[1].header['NAXIS2'])

    self.parent_directory = parent_directory

    self.z={}
    self.matches={}
    self.sigma_z={}
    
    for pointingID in self.pointingID_list:
      for det in self.detlist:
	rootdir = self.parent_directory+'pointing%d/E2ES_output/DETECTOR_%s/spectra_analysis/'%(pointingID,det)
	for subdir, dirs, files in os.walk(rootdir):
	  for ff in files:
	    zExt=pickle.load(open(subdir+ff,'rb'))
	    key = str(pointingID)+'_'+ff.split('.')[0]
	    self.z[key]=zExt.tab['z']
	    self.matches[key]=zExt.tab['matches']
	    self.sigma_z[key]=(zExt.tab['eff_detected_lines_fwhm']/self.sigma2fwhm)/6563.

  def plot_hit(self,catalog, catalog_directory = './catalog/', directory='./'):
    
    import numpy as np
    import matplotlib.pyplot as plt
    import pyfits
    
    self.catalog = catalog
    self.catalog_directory = catalog_directory
    
    ff=pyfits.open(catalog_directory+catalog)
    try:
      ra = ff[1].data['RA']
      dec = ff[1].data['DEC']
    except:
      ra = ff[1].data['alpha']
      dec = ff[1].data['delta']
      
    plt.figure()
    plt.plot(ra,dec,'bo')

    hit = []    
    for key in self.z:
      hit.append(int(key.split('_')[-1][:-1]))
    hit=np.unique(hit)
    
    for pointingID in self.pointingID_list:
      for key in self.z:
	if key.split('_')[0]==str(pointingID):
	  hit=int(key.split('_')[-1][:-1])
	  plt.plot(ra[hit-1],dec[hit-1],'.',c=plt.cm.rainbow(1./pointingID))

    plt.xlabel('RA [deg]')
    plt.ylabel('DEC [deg]')
    plt.title('Hit')
    plt.savefig(directory+'plot_hit.png')

  def plot_z_per_pointing(self, directory='./'):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from collections import OrderedDict
    
    plt.figure()

    for pointingID in self.pointingID_list:
      for key in self.z:
	if key.split('_')[0]==str(pointingID):
	  n = int(key.split('_')[-1][:-1])
	  x = np.ones(len(self.z[key]))*n
	  plt.errorbar(x,self.z[key],yerr=self.sigma_z[key],fmt='o',c=plt.cm.rainbow(1./pointingID),label='pointing %d'%pointingID)
	  
    # A little trick to avoid redundancy in the legend because I am labelling within a loop
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))

    plt.plot([0,self.N],[self.reference_z,self.reference_z],'k-')
    plt.xlim((-1,self.N+1))
    plt.xlabel('Source ID')
    plt.ylabel('Redshift')
    plt.title('Pointings')
    plt.legend(by_label.values(), by_label.keys(),loc=2)
    plt.savefig(directory+'plot_pointings.png')
    
  def plot_matches(self, directory='./'):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from collections import OrderedDict
    
    plt.figure()

    for pointingID in self.pointingID_list:
      for key in self.z:
	n = key.split('_')[-1][:-1]
	n=int(n)
	x = np.ones(len(self.z[key]))*n
	if len(self.z[key])==1:
	  if self.matches[key][0] == 1:
	    plt.errorbar(x,self.z[key],yerr=self.sigma_z[key],fmt='bo',label='len(z)=1, matches=1')
	  else:
	    plt.errorbar(x,self.z[key],yerr=self.sigma_z[key],fmt='c^',label='len(z)=1, matches>1')      
	else:
	  for j in range(len(self.z[key])):
	    if self.matches[key][j] == 1:
	      plt.errorbar(x,self.z[key],yerr=self.sigma_z[key],fmt='ro',label='len(z)>1, matches=1')
	    else:
	      plt.errorbar(x,self.z[key],yerr=self.sigma_z[key],fmt='y^',label='len(z)>1, matches>1')

    # A little trick to avoid redundancy in the legend because I am labelling within a loop
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))

    plt.plot([0,self.N],[self.reference_z,self.reference_z],'k-')
    plt.xlim((-1,self.N+1))
    plt.xlabel('Source ID')
    plt.ylabel('Redshift')
    plt.title('Matches')
    plt.legend(by_label.values(), by_label.keys(),loc=2)
    plt.savefig(directory+'plot_matches.png')

  def plot_residuals(self, directory='./'):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from collections import OrderedDict
    
    plt.figure()

    for pointingID in self.pointingID_list:
      for key in self.z:
	if key.split('_')[0]==str(pointingID):
	  n = int(key.split('_')[-1][:-1])
	  x = np.ones(len(self.z[key]))*n
	  ref = np.ones(len(self.z[key]))*self.reference_z
	  for i in range(len(x)):
	    if self.z[key][i]-ref[i] < 0:
	      plt.plot([x[i],x[i]],[self.z[key][i]-ref[i],0],'b.-',label='negative residual')
	    else:
	      plt.plot([x[i],x[i]],[self.z[key][i]-ref[i],0],'r.-',label='positive residual')

    # A little trick to avoid redundancy in the legend because I am labelling within a loop
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))

    plt.plot([0,self.N],[0.,0.],'k-')
    plt.xlim((-1,self.N+1))
    plt.xlabel('Source ID')
    plt.ylabel('Redshift')
    plt.title('Residuals')
    plt.legend(by_label.values(), by_label.keys(),loc=2)
    plt.savefig(directory+'plot_residuals.png')
    
  def evaluate_statistics(self, file_name, directory = './'):
    
    import numpy as np
    import copy
    from csv_table import csv_table
    #from fits_handler import dict2fits
    
    reqs = csv_table('./auxiliary/Performance_Assessment_Module/scientific_requirements.csv')
            
    to_search = np.arange(1,self.N+1)
    to_search = to_search.astype(str)
    
    stat_file = open(directory+file_name,'w+')
    
    header = 'completeness,'    
    header+= 'completeness_requirement,'
    header+= 'purity_partial,'
    header+= 'purity,'
    header+= 'purity_requirement,'
    header+= 'sigma_z_min,'
    header+= 'sigma_z_max,'
    header+= 'sigma_z_mean,'
    header+= 'sigma_z_reference,'
    header+= 'sigma_z_min_requirement,'
    header+= 'sigma_z_max_requirement'
    
    stat_file.write(header+'\n')
    
    self.detected = []
    for n in to_search:
      for key in self.z:
	if key.split('_')[-1][0:-1]==n and len(self.z[key]) != 0:
	  self.detected.append(key.split('_')[-1][0:-1])
    self.detected=np.unique(self.detected)
    line = '%f,'%(len(self.detected)/float(self.N))  
    line+= '%s,'%reqs['completeness'][0]
    
    counter_par=0
    for key in self.z:
      if len(self.z[key])==1: 
	for z in self.z[key]:
	  if abs(z-self.reference_z)<0.01:
	    counter_par+=1
    line+= '%f,'%(counter_par/float(len(self.detected)))    
        
    counter=0
    for key in self.z:
      if len(self.z[key])>0: 
	for z in self.z[key]:
	  if abs(z-self.reference_z)<0.01:
	    counter+=1
    line+= '%f,'%(counter/float(len(self.detected)))
    line+= '%s,'%reqs['purity'][0]
    
    sigma_z_list=[]
    ref_sigma_z_list=[]
    z_list=[]
    for key in self.sigma_z:
      for sigma in self.sigma_z[key]:
	sigma_z_list.append(sigma)
      for z in self.z[key]:
	ref_sigma_z_list.append(reqs['z_error'][0]*(1+z))
	z_list.append(z)
    sigma_z_list=np.array(sigma_z_list)
    ref_sigma_z_list=np.array(ref_sigma_z_list)
    z_list=np.array(z_list)
        
    line+= '%f,'%sigma_z_list.min()
    line+= '%f,'%sigma_z_list.max()
    line+= '%f,'%sigma_z_list.mean()
    line+= '%f,'%(float(reqs['z_error'][0])*(1+self.reference_z))
    line+= '%f,'%(float(reqs['z_error'][0])*(1+float(reqs['z_min'][0])))
    line+= '%f,'%(float(reqs['z_error'][0])*(1+float(reqs['z_max'][0])))
    
    stat_file.write(line+'\n')
    stat_file.close()
    
    #Stat={}
    #Stat['z']=z_list
    #Stat['sigma_z']=sigma_z_list
    #Stat['ref_sigma_z'] = ref_sigma_z_list
    #dict2fits(Stat,'stat.fits')
