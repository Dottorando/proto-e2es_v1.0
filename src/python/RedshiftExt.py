class redshift_extraction:
  
  def __init__(self, spectrum):
    self.spectrum = spectrum

  def ideIRAF(self):
    from pyraf import iraf
    from csv_table import csv_table
    import os
    import string
    # define parameters
    
    params = csv_table('./auxiliary/iraf/ideIRAF_parameters.csv')
    MAXFEATURES = params['MAXFEATURES'][0]
    FEATURE_THRES = float(params['FEATURE_THRES'][0])
    FWIDTH= float(params['FWIDTH'][0]) # full width half maximum in pixels of fetaures to be iraf-identified Not really understoood, cf identify
    MATCH= float(params['MATCH'][0]) # max difference for a match between feature coords and a coord in coords list in user corrds units. Not really understoood, cf identify
    MINSEP= int(params['MINSEP'][0])
    
    CURSORCOMMAND = './auxiliary/iraf/'+'y.dat'
    LINEFILE = './auxiliary/iraf/'+'Righe.dat'
    
    # find lines using iraf identify  
    self.results=[]
    
    #NON HO BEN CAPITO A CHE SERVE
    iraf.set(stdvdm="vdm.out")
    
    devnull=open(os.devnull,'w')
    iraf.noao.onedspec.identify(self.spectrum, ftype='emission',maxfeature=MAXFEATURES ,threshold=FEATURE_THRES, \
				fwidth=FWIDTH ,match=MATCH , minsep=MINSEP, graphics='stdvdm' , \
				cursor=CURSORCOMMAND ,autowrite='yes', \
				database='database', coordlist=LINEFILE, Stdout=devnull, Stderr=devnull)
	# WARNING: anche qui, viene dato un output a terminale da iraf
    devnull.close()
    # read iraf output file
    #output file is named idXXXX
    # where XXXX.fits is the input file
    fileRootName=self.spectrum.split('.')[0]
    print fileRootName
    irafOutName='id'+fileRootName
    irafOut=open('database/'+irafOutName)
    l=irafOut.readlines()
    irafOut.close()
    # output contains 10 header lines, then pixel and lambda coords of identified lines 
    for i in range(10,len(l)-1):
      tmp=string.split(l[i])
      lambdaOss=float(tmp[1])
      self.results.append(lambdaOss)
      

  def Continuum(self):

    from pyraf import iraf
    import os
    import numpy as np
    from fits_handler import mono2LambdaFlux
    #from fase.fileio import Spectrum #NON CE L'ABBIAMO!!!!!

    STEPLIMIT=10
    #LAMBDA_MIN=9000. # Paolo lo hardcodava, io glieli do dal max e min del range su cui cerca (pare meno grezzo)
    #LAMBDA_MAX=25000. # Paolo lo hardcodava, io glieli do dal max e min del range su cui cerca (pare meno grezzo)
    # find lines extremes using iraf task continuum
    self.ranges=[]

    # RECUPERO LAMBDA E FLUX DALL'IMMAGINE MONODIMENSIONALE
    _flux, _lambda = mono2LambdaFlux(self.spectrum)
    FLUX = _flux[0]    
    LAMBDA = _lambda[0]

    # fit a spline to continuum     
    tmp_file = self.spectrum.split('.')[0]+'_CONT'+'.fits'
    iraf.noao.onedspec.continuum(self.spectrum, function="spline3",order=4,type="fit",\
				interact="no",output=tmp_file,naverage=1)

    # RECUPERO LAMBDA E FLUX DALL'IMMAGINE MONODIMENSIONALE DOPO IL PASSAGGIO PER CONTINUUM
    _flux_cont, _lambda_cont = mono2LambdaFlux(tmp_file)
    FLUX_CONT = _flux_cont[0]
    LAMBDA_CONT = _lambda_cont[0]

    LAMBDA_MIN=LAMBDA_CONT.min()-1000.
    LAMBDA_MAX=LAMBDA_CONT.max()+1000.
    
    # for each line which has been detected by identify, find the extremes
    for i in range(0,len(self.results)):
      obsLine=self.results[i]
      obsLine=self.results[i]
      # find the flux values corresponding to obsLine: take the pixel at lambda >= ideIraf_lambda (first index of the where!)
      peakIndex=np.where(LAMBDA >= obsLine)[0][0]
      x0=LAMBDA_CONT[peakIndex]
      y0=FLUX_CONT[peakIndex]
      ymax=FLUX[peakIndex]
      # y0 is the peak value on the cont.subtracted spectrum, ymax is the peak value on the original spectrum
      # now go to the left of the peak and find line start
      # in a VERY ROUGH WAY!!!!!
      x1=-99
      for i in range(1,STEPLIMIT):
	j=peakIndex-i
	if (j >=0):
	  x1=LAMBDA[j]
	  y1=FLUX[j]	
	else:
	  x1=LAMBDA[0]
	  y1=FLUX[0]
	# when the flux in the original spectrum is less than the peak continuum subtracted flux 
	# I have found the line start (MAH!)
	# Note that it does not go beyond stepLimit!
	if y1 < y0:
	  break
      # and repeat for line end 
      x2=-99
      for i in range(1,STEPLIMIT):
	j=peakIndex+i
	if (j < len(LAMBDA)):
	  x2=LAMBDA[j]
	  y2=FLUX[j]	
	else:
	  x2=LAMBDA[-1]
	  y2=FLUX[-1]	
	# when the flux in the original spectrum is less than the peak continuum subtracted flux
	# I have found the line end (MAH!)
	# Note that it does not go beyond stepLimit!
	if y2 < y0:
	  break      
      # in the original code, there is an extremely
      # complicated piece of code maybe meant to refine line start and end, or the continuum under the peak?
      # I do not duplicate it here, because I think it is wrong
      # for the line refinement, splot is smart enough, provided reasonable guesses are given
      if x1 < 0:
	x1=LAMBDA_MIN
      if x2 < 0:
	x2=LAMBDA_MAX
      # in the original code, the continuum flux under the peak 
      # is passed. Which is the continuum level splot
      # uses for continuum (not fitted). 
      # but if it is negative, force to 0, so that EW is not indef
      # splot reasults are very sensitive to this level
      if y0 <= 0 :
	y0 =0
      self.ranges.append((x1,x2,y1,y2))


  def Splot(self):
    
    # use splot,  option k to derive useful parameters 
    # WARNING: splot is VERY SENSITIVE to where the continuum is placed
    # and in the original code it was placed at level of the fitted continuum at the peak 
    # IN any case, our fit (like in sgnaps or sviewer) works much better because it fits the local continuum
    # it would be worth to change this 
    # also changed, so that splot is run only once, and not once per line

    from pyraf import iraf
    import os
    import string
    import numpy as np
    from collections import OrderedDict

    CURSORCOMMAND = './auxiliary/iraf/'+'Cursor.dat'
    SPLOTLOG = './auxiliary/iraf/'+'splot.log'

    # cleaning is repeated also before calling splot again: sometimes, something remains around from previous run
    try:
      os.remove(SPLOTLOG)
      os.remove(CURSORCOMMAND)
    except:
      pass
	
    #NON HO BEN CAPITO A CHE SERVE
    iraf.set(stdvdm="vdm.out")
	
    splotFile=open(CURSORCOMMAND,'w')
    for i in range(0,len(self.ranges)):
      lam1=self.ranges[i][0]
      lam2=self.ranges[i][1]
      y1=self.ranges[i][2]
      y2=self.ranges[i][3]
      comandi='%f %g 1 k \n %f %g 1 k \n' % (lam1,y1,lam2,y2)
      splotFile.write(comandi)
    splotFile.write('q \n')
    splotFile.close()
    # a splot viene passata (attraverso il file Cursor.dat) una riga di comandi del tipo
    # lambdaStart Fcont 1 k
    # lambdaEnd Fcont 1 k
    # che vuol dire fitta una gaussiana da lambdaStart e lambdaEnd con il continuo al livello di Fcont
    # WARNING documentazione in linea dice che il continuo dovrebbe venir normalizzato a 1
    # WARNING: fa l'eco dei comandi di splot a terminale!! e sembra che si incasini spesso e volentieri
    # WARINING: sarebbe anche bene togliere dai piedi il salvatggio in eps files, che rallenta e forse
    # e' quello che da problemi. Ma le istruzioni trovate in rete non vanno, perche' in iraf non e' definito dev$null
      
    devnull=open(os.devnull,'w')
    iraf.noao.onedspec.splot(self.spectrum,cursor=CURSORCOMMAND, \
			    options='nosysid',save_fi=SPLOTLOG,graphics='stdvdm', Stdout=devnull, Stderr=devnull)
    devnull.close()

    irafOut=open(SPLOTLOG,'r')      
    l=irafOut.readlines()
    irafOut.close()   
    
    # clean iraf files
    #try:
      #os.remove("splot.log")
      #os.remove("logfile")
      #os.remove("Cursor.dat")
    #except:
      #pass

    #MY VERSION

    self.listlin=OrderedDict()
    centerLam=[]
    totFlux=[]
    ew=[]
    peakFlux=[]
    gfwhm=[]
    deltaLam=[]

    # THE HEADER IS MADE OF 3 LINES
    lcut = np.delete(l,np.arange(3))

    for i in range(0,len(lcut)):
      tmp=string.split(lcut[i])
      # do not accept undefined lambda
      if string.find(tmp[0],'INDEF') >=0:
	centerLam.append(0.)
      else:
	centerLam.append(float(tmp[0]))
      # I don't use the cont variable but it looks like it don't like a INDEF value anyway
      if string.find(tmp[1],'INDEF') >=0:
	continue
      # do not accept undefined tot flux
      if string.find(tmp[2],'INDEF') >=0:
	totFlux.append(0.)
      else:
	totFlux.append(float(tmp[2]))
      if string.find(tmp[3],'INDEF') >=0:
	ew.append(0.)
      else:
	ew.append(float(tmp[3]))
      if string.find(tmp[4],'INDEF') >=0:
	peakFlux.append(0.)
      else:
	peakFlux.append(float(tmp[4]))
      # do not accept undefined gwhm
      if string.find(tmp[5],'INDEF') >=0:
	gfwhm.append(0.)
      else:
	gfwhm.append(float(tmp[5]))

      deltaLam.append(self.ranges[i][1]-self.ranges[i][0])
    self.listlin['centerLam']=centerLam
    self.listlin['gfwhm']=gfwhm
    self.listlin['totFlux']=totFlux
    self.listlin['ew']=ew
    self.listlin['peakFlux']=peakFlux
    self.listlin['deltaLam']=deltaLam

  def RedshiftMeasure(self,delta_lambda = None):

    # INDEF values have already been excluded from the list 				
    # OSS: EW < 0 VUOL DIRE CHE HO RIGHE DI EMISSIONE
    
    from collections import OrderedDict
    import string
    import numpy as np
    import operator
    from csv_table import csv_table
    
    MAXFWHM=30.

    #First of all I load a dict with the lines list
    LINEFILE = './auxiliary/iraf/'+'Righe.dat'
    
    self.ref_lines = OrderedDict()
    
    tmp = open(LINEFILE,'r')
    ref=tmp.readlines()
    tmp.close
    ref_cut = np.delete(ref,0)
    for i in range(len(ref_cut)):
      line = string.split(ref_cut[i])
      key = line[1]
      self.ref_lines[key]=float(line[0])
    #sort_ref_lines = sorted(ref_lines.items(), key=operator.itemgetter(1),reverse = True)
    
    cl=[]

    # salva lambda e EW delle righe con fwhm < di un massimo prefissato e EW negativo
    # la lambda in origine era quella di identify, invece io uso quella di splot
    # Inoltre, tiene traccia di fwhm, flusso e lambda della
    # riga col flusso piu' alto, purche' con 
    # EW < 0 e fwhm < dei limiti della riga e flusso positivo   
    lambdaTmp=0
    fluxTmp=0
    ggf=0
    for j in range(0,len(self.listlin['gfwhm'])):
      if (self.listlin['totFlux'][j] > fluxTmp and self.listlin['gfwhm'][j] <= self.listlin['deltaLam'][j] and self.listlin['ew'][j] < 0.):
	ggf=self.listlin['gfwhm'][j]
	fluxTmp=self.listlin['totFlux'][j]
	lambdaTmp=self.listlin['centerLam'][j]
      if (self.listlin['gfwhm'][j] > MAXFWHM and self.listlin['ew'][j] < 0.):
	cl.append((self.listlin['centerLam'][j],self.listlin['gfwhm'][j]))
    # ordina le righe per lambda descresente
    
    cleanlist=sorted(cl,key=lambda cl: cl[0], reverse=True)

    # ESPERIMENTO
    eff_detected_lines = []
    eff_detected_lines_fwhm = []
    for pair in cleanlist:
      eff_detected_lines.append(pair[0])
      eff_detected_lines_fwhm.append(pair[1])
    eff_detected_lines = np.array(eff_detected_lines)
    eff_detected_lines_fwhm = np.array(eff_detected_lines_fwhm)
    
    #rest_ref = []
    #for n in range(len(sort_ref_lines)):
      #rest_ref.append(sort_ref_lines[n][1])
    #rest_ref = np.array(rest_ref)
    
    self.tab = OrderedDict()
    self.tab['eff_detected_lines'] = eff_detected_lines
    self.tab['eff_detected_lines_fwhm'] = eff_detected_lines_fwhm
    
    # calculate preliminary z, assuming that the detected line is the H_alpha (I do it for all the detected lines!)
    try:
      z = eff_detected_lines/self.ref_lines['H_ALPHA']-np.ones(len(eff_detected_lines))
    except:
      z = eff_detected_lines/self.ref_lines['H_alpha']-np.ones(len(eff_detected_lines))
    self.tab['z'] = z
    
    # shifts the rest_ref to the calculated redshifts
    for nome_riga in self.ref_lines.keys():
      self.tab[nome_riga+'_shifted']=self.ref_lines[nome_riga]*(np.ones(len(eff_detected_lines))+z)
    
    # look for matches
    to_match = self.tab['eff_detected_lines']
    matches = []
    if delta_lambda == None:
      for iz in range(len(self.tab['z'])):
	match=0
	for lam in to_match:
	  for nome_riga in self.ref_lines.keys():
	    if (abs(lam-self.tab[nome_riga+'_shifted'][iz]) <= 2*self.tab['eff_detected_lines_fwhm'][iz]):
	      match+=1
	matches.append(match)
      self.tab['matches'] = matches
    else:
      for iz in range(len(self.tab['z'])):
	match=0
	for lam in to_match:
	  for nome_riga in self.ref_lines.keys():
	    if (abs(lam-self.tab[nome_riga+'_shifted'][iz]) <= 2*delta_lambda):
	      match+=1
	matches.append(match)
      self.tab['matches'] = matches
    
    # cleanlist e' ordinato in lambda decrescente, e ha lambda in 0, fwhm in 1
    #result = None
    #red_shifted_lines = OrderedDict()
    #for j in range(0,len(cleanlist)):
      # assumo che la riga sia halpha, e calcolo z, nonche' la lambda di o2,o3,Hb
      #l=cleanlist[j][0]
      #fw=cleanlist[j][1]
      #try:
	#z=(l-ref_lines['H_ALPHA'])/ref_lines['H_ALPHA']
      #except:
	#z=(l-ref_lines['H_alpha'])/ref_lines['H_alpha']      
      #print z
      #for linekey in ref_lines:
	#red_shifted_lines[linekey+'_redshifted']=(1.+z)*ref_lines[linekey]
	
      #for k in range(j+1,len(cleanlist)):
	# scorre la lista di righe: se la riga ha fwhm < di quella di riferimento
	# e se ha lambda compatibile  con la lambda aspettata per o2, hb, o3a 
	# salva il risultato (pero' 1 solo, perche' non appende) ma poi non mi pare venga mai usato    
	#print fw
	#if cleanlist[k][1] < fw:	
	  #if (cleanlist[k][0] - o3a) <= deltaLambda:
	    #result=(cleanlist[k][0],'OIIIa')
	  #if (cleanlist[k][0] - hbeta) <= deltaLambda:
	    #result=(cleanlist[k][0],'H_be')																	
	  #if (cleanlist[k][0] - o2) <= deltaLambda:
	    #result=(cleanlist[k][0],'OII')    
	## non appena trova un'altra riga, ritorna la lambda della supposta halpha, nonche' la lambda dell'altra riga e la
	## sua identificazione
	##a questo punto, result e' una tupla di tuple, e.g. (15000,(12000,o2))
	##Il che e' delirio, perche' non testa tutte le possibili combinazioni
	#if (result is not None) :
	  #result=(l,result)
	  ##a questo punto, result e' una tupla di tuple, e.g. (15000,(12000,o2))
	  #return result
	##passo da qui solo se non ho trovato matches        
    #result=(0,'')
    #result=(0,result)
    #print result
    #return result      
