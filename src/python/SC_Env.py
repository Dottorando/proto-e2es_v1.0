class SpacecraftEnvironment:
  
  def __init__(self, OTL_file, directory='./'):
    
    from csv_table import csv_table

    self.OperationalTimeLine_file_name = OTL_file
    self.OperationalTimeLine = csv_table(directory+OTL_file)

  def get_tables_names(self):
    
    return self.OperationalTimeLine.keys()

  def get_instrument_timeline(self, instrument):
    
    return self.OperationalTimeLine.select(INSTRUMENT = instrument)
  
  def get_dither_timeline(self, dither):
    
    return self.OperationalTimeLine.select(POINTING = 'dither %d'%dither)
  
  def get_activity_timeline(self, payload):
    
    if (payload != 'VIS') and (payload != 'NISP') and (payload != 'S/C'):
      
      print 'Allowed values for payload: VIS, NISP or S/C'
    
    return self.OperationalTimeLine.select(ACTIVITY = payload)

  def get_activity_sequence(self):
    
    return self.OperationalTimeLine['ACTIVITY']
  
  def get_action_sequence(self):
    
    return self.OperationalTimeLine['ACTION']