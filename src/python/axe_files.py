class FileGenerator:

  def __init__(self, catalog):
    self.catalog = catalog

  def create_dat(self,dither,band,slitless_image,dat_name = 'input.dat',directory = './', conf_fits_file = 'tips_configuration.fits'):
    
    import tips
    import numpy as np
    import pyfits
    from csv_table import csv_table
    from catalog_handler import CatalogHandler
    from Mdb import Mdb    

    self.slitless_image = slitless_image
    self.directory = directory
    self.dat_name = dat_name

    # GET SLITLESS IMAGE INFOS
    slitim = pyfits.open(self.directory+self.slitless_image)
    slitim_info = slitim[1].header
    
    ff=pyfits.open('./conf/'+conf_fits_file)   
    NPIX = ff[0].header['NPIXX']
    PIXSIZE = ff[0].header['PIXSIZE']

    CH = CatalogHandler(self.catalog,directory = './catalog/')
    wcs = tips.WCSObject()    
    wcs.updateWCS(pixel_scale=0.3, orient=0.0, \
                  refpos=[slitim_info['CRPIX1'],slitim_info['CRPIX2']], \
                  refval=[slitim_info['CRVAL1'],slitim_info['CRVAL2']], \
                  size=[slitim_info['NAXIS1'],slitim_info['NAXIS2']])
    
    axinp = open(self.directory+self.dat_name, 'w+')
    # start writing the catalog
    axinp.write('# 1  NUMBER\n')
    axinp.write('# 2  X_IMAGE\n')
    axinp.write('# 3  Y_IMAGE\n')
    axinp.write('# 4  X_WORLD\n')
    axinp.write('# 5  Y_WORLD\n')
    axinp.write('# 6  A_IMAGE\n')
    axinp.write('# 7  B_IMAGE\n')
    axinp.write('# 8  THETA_IMAGE\n')
    axinp.write('# 9  A_WORLD\n')
    axinp.write('# 10  B_WORLD\n')
    axinp.write('# 11  THETA_WORLD\n')
    
    # CARICO IL FILTRO
    filters = csv_table('./auxiliary/axe/Filters.csv')
    ii = np.where(filters['band'] == band)[0]    
    axinp.write('# 12  MAG_%s%4d\n' %(filters['band'][ii][0],filters['wavelength'][ii][0])) # the first character of the filter name MUST be a magnitude label

    # GET THE EE80 FOR THE BAND WAVELENGHT
    MissionDB = Mdb('./auxiliary/Euclid_Mission_DB/SpaceSegment/Instrument/NISP/NISPAsDesigned/EUC-TEST-SPACESEGMENT-INSTRUMENT-NISP-NISPASDESIGNED.xml')
    EE80_values = MissionDB.getValue('SpaceSegment.Instrument.NISP.NISPAsDesigned.NISPTotalSREE80PDR')
    wl=[]
    EE=[]
    for i in range(len(EE80_values)):
      wl.append(EE80_values[i][0])
      EE.append(EE80_values[i][1])    

    EE80 = np.interp(filters['wavelength'][ii][0],wl,EE)
    
    #CARICO LA ROTAZIONE
    ditrot = csv_table('./auxiliary/axe/DithersRot.csv')
    ii = np.where(ditrot['ditherID'] == dither)[0]    
        
    ids = CH.catalog_dict['num']    
    for _id in ids:
      data = CH.get_object(_id)
      ra = data['alpha']
      dec = data['delta']
      # TIPS has been used to simulate image; use its routines for the WCS
      x_image_tmp,y_image_tmp = wcs.rd2xy(ra,dec)
      #if the image is rotated the coordinates must be rotated
      if ditrot['rot90'][ii][0] == 'T':
	x_image = NPIX-y_image_tmp
	y_image = x_image_tmp
      else:
	x_image = x_image_tmp
	y_image = y_image_tmp
	
      mag = data['mag']
      template_name = data['incident']
      z = data['z']
      rbulge = data['rbulge']
      rdisk = data['rdisk']
      bt = data['bt']
      pa = data['pa']
      axratio = data['axratio']
      
      if bt>=0:
	# object dimensions
	obj_diam = 2*np.sqrt(rbulge**2+(1-bt)*rdisk**2) # arcsec 	
	obj_diam_convolved = np.sqrt(obj_diam**2+(EE80*2)**2) # arcsec
	A = obj_diam_convolved/PIXSIZE/2.0  # the extraction aperture is A * 1 * 2 (1 is mfwhm)
	B = A # * axratio - always a perpendicular extraction (no B = A*axratio)			
      else:	
	# point-like objects
	A = EE80/PIXSIZE
	B = A
	pa = 0.0

      axinp.write('%d %f %f %f %f %.3f %.3f %.3f %.3f %.3f %.3f %.3f' % (_id,x_image,y_image,ra,dec,A,B,pa,A,B,pa,mag)) 
      axinp.write('\n')      
    axinp.close()

  def create_conf_files(self,dither,fits_file_in,conf_file_name = 'axe.conf',conf_directory = './conf/'):
    
    import pyfits
    from fits_handler import split_components
    
    self.dither = dither
    self.fits_file_in = fits_file_in
    self.conf_file_name = conf_file_name
    self.conf_directory = conf_directory
    """
    Create the aXe configuration file
    """
    conff = open(self.conf_directory+self.conf_file_name,'w+')
    ff = pyfits.open(self.conf_directory+self.fits_file_in)
    
    pconf = ff['PRIMARY'].header
    dkey = pconf['EXPO%d'%self.dither]
    dconf = ff[dkey].header
    
    conff.write('INSTRUMENT DUMMY\n')
    conff.write('CAMERA DUMMY\n')
    conff.write('SCIENCE_EXT SCI\n')
    conff.write('DQ_EXT DQ\n')
    conff.write('ERRORS_EXT ERR\n')
    conff.write('FFNAME None\n')
    conff.write('RDNOISE %d\n' %pconf['RN'])
    conff.write('EXPTIME EXPTIME\n')
    conff.write('POBJSIZE 0.5\n')
    conff.write('SMFACTOR 1.0\n')
    conff.write('#\n')
    #
    #beam
    #
    #order 1
    #
    conff.write('# Order 1 (BEAM %s)\n' % dconf['BEAMID0'])
    conff.write('#\n')
    conff.write('BEAMA %d %d\n' % (dconf['BSTART0'],dconf['BEND0']))
    conff.write('MMAG_EXTRACT_A 35\n')
    conff.write('MMAG_MARK_A 35\n')
    conff.write('#\n')
    conff.write('# Trace description\n')
    conff.write('#\n')
    conff.write('DYDX_ORDER_A %d\n' % dconf['ODYDX0'])
    for i in range(dconf['ODYDX0']+1):
      conff.write('DYDX_A_%d %g\n' % (i,dconf['DYDX0_%d' % i]))
    conff.write('#\n')
    conff.write('# X and Y Offsets\n')
    conff.write('#\n')
    conff.write('XOFF_A %f\n' % dconf['XOFF0'])
    conff.write('YOFF_A %f\n' % dconf['YOFF0'])
    conff.write('#\n')
    conff.write('# Dispersion solution\n')
    conff.write('#\n')
    conff.write('DISP_ORDER_A %d\n' % dconf['ODLDP0'])
    for i in range(dconf['ODLDP0']+1):
      conff.write('DLDP_A_%d %g\n' % (i,dconf['DLDP0_%d' % i]))
    conff.write('#\n')
    
    # Create a fits file containing only the beam sensitivity
    split_components(self.fits_file_in,component=dconf['SENS0'],directory=self.conf_directory)
    
    conff.write('SENSITIVITY_A %s%s\n'%(self.conf_directory,self.fits_file_in.split('.')[0]+'_'+dconf['SENS0']+'.fits'))
    conff.write('#\n')
    conff.close()
