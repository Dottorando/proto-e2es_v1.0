import tips

class NISP_S:

  def __init__(self,input_file,inDir = './', outDir = './'):
    self.input_file = input_file
    self.inDir = inDir
    self.outDir = outDir
    self.inSpcForm = 'SplitFits' #HARDCODED
    self.inCatForm = 'TIPS' #HARDCODED
    self.inThmForm = 'SplitFits' #HARDCODED
  
  def get_pointing(self,ra0, dec0):
    self.ra0 = ra0
    self.dec0 = dec0

  #def get_pointing_center(self):
    #from fits_handler import fits2dict
    #import pyfits
    #try:
      #table_key = pyfits.open(self.inDir+self.input_file)[1].header['EXTNAME']
    #except:
      #table_key = ''
    #print table_key
    #_dict = fits2dict(self.inDir+self.input_file,table_key)
    #try:
      #self.ra0 = _dict['RA'].min()+(_dict['RA'].max()-_dict['RA'].min())/2.
      #self.dec0 = _dict['DEC'].min()+(_dict['DEC'].max()-_dict['DEC'].min())/2.
    #except:
      #self.ra0 = _dict['alpha'].min()+(_dict['alpha'].max()-_dict['alpha'].min())/2.
      #self.dec0 = _dict['delta'].min()+(_dict['delta'].max()-_dict['delta'].min())/2.
      
  def load_TipsEuclidDefault(self, grismName, exptime = 560.):
    self.grismName = grismName
    self.exptime = exptime
    self.obs = tips.Observation(self.inDir+self.input_file, inSpcForm=self.inSpcForm, inCatForm=self.inCatForm, inThmForm=self.inThmForm, silent=False)
    self.obs.loadEUCLIDDefault(grismName=self.grismName, exptime=self.exptime, ra0=self.ra0, dec0=self.dec0)

  def load_CustomConfig(self,ConfigFileName,exptime,conf_dir = './conf/'):
    import pyfits
    self.ConfigFileName = ConfigFileName
    self.conf_dir = conf_dir
    self.exptime = exptime
    self.obs = tips.Observation(self.inDir+self.input_file, inSpcForm=self.inSpcForm, inCatForm=self.inCatForm, inThmForm=self.inThmForm, silent=False)
    self.obs.loadFromFile(self.conf_dir+self.ConfigFileName,exptime=self.exptime,ra0=self.ra0, dec0=self.dec0, x0 = 1.0, y0 = 1.0)

  def run_Simulation(self):
    self.obs.runSimulation(workDir=self.outDir)
