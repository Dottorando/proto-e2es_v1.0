class ZodiMaps:
  
  def __init__(self,wavelength, minday, maxday, elongmin, elongmax,):
    
    import numpy as np

    self.wavelength = wavelength
    self.minday = minday
    self.maxday = maxday
    self.elongmin = elongmin
    self.elongmax = elongmax
    self.epsilon=np.deg2rad(23.439)

  def get_zodi_corot_mean(self, directory = './'):

    from corot_map_edit import fits2corot_mean

    self.cmhr=fits2corot_mean(directory+'zody_corot_mean_nside=64_w=%3.2f.fits'%self.wavelength)
        
  def initialize_maps(self):

    from grid2d import MapGrid,GridAxis
    import numpy as np
    
    outmaps = ['flux','side','elongation']

    self.MG=MapGrid(GridAxis('eq_RA','deg',0.,360.,1.),GridAxis('eq_DEC','deg',-90.,90.,1.))

    self.MG.newmap(self.MG.C['name'],value=self.MG['_col_values'].copy(),unit=self.MG.C['unit'])
    self.MG.newmap(self.MG.R['name'],value=self.MG['_row_values'].copy(),unit=self.MG.R['unit'])

    self.MG.newmap('Xeq',value=np.cos(np.deg2rad(self.MG['eq_RA']))*np.cos(np.deg2rad(self.MG['eq_DEC'])))
    self.MG.newmap('Yeq',value=np.sin(np.deg2rad(self.MG['eq_RA']))*np.cos(np.deg2rad(self.MG['eq_DEC'])))
    self.MG.newmap('Zeq',value=np.sin(np.deg2rad(self.MG['eq_DEC'])))

    for outm in outmaps:
      self.MG.newmap(outm,value=self.MG.zeros())
      
  def calculate_daily_elongations(self,directory='./'):
    
    import pyfits
    import numpy as np
    from corot_map_edit import corot_map,fits2corot_mean,fits2corot_day
    
    hdulist = []
    hdulist.append(pyfits.PrimaryHDU())

    for day in range(self.minday,self.maxday+1):
      hdulist.append([])
      #corotating map for a given longitude
      cmd=self.cmhr.corot_map_at_epoch(float(day))
      sun_ecl=np.array([
      np.cos(np.deg2rad(cmd.solar_longitude_deg)),
      np.sin(np.deg2rad(cmd.solar_longitude_deg)),
      0])
      #Components of the Sun vector in equatorial coordinates
      sun_xeq=sun_ecl[0]
      sun_yeq=np.cos(self.epsilon)*sun_ecl[1]-np.sin(self.epsilon)*sun_ecl[2]
      sun_zeq=np.sin(self.epsilon)*sun_ecl[1]+np.cos(self.epsilon)*sun_ecl[2]
      #Mappa elongazione. arcos (prodotto scalare tra punto mappa e Sole in coord. eq)
      self.MG['elongation']=np.rad2deg(np.arccos(self.MG['Xeq']*sun_xeq+self.MG['Yeq']*sun_yeq+self.MG['Zeq']*sun_zeq))
      hdulist[-1] = pyfits.ImageHDU(self.MG['elongation'],name=str(day))

    thdulist=pyfits.HDUList(hdulist)
    fits_name = 'Zody_daily_elongations_days%d-%d.fits'%(self.minday,self.maxday)
    thdulist.writeto(directory+fits_name,clobber=True)
        
  def calculate_daily_flux(self,directory='./'):
    
    import pyfits
    import numpy as np
    from corot_map_edit import corot_map,fits2corot_mean,fits2corot_day
    
    hdulist = []
    hdulist.append(pyfits.PrimaryHDU())

    for day in range(self.minday,self.maxday+1):
      hdulist.append([])
      #corotating map for a given longitude
      cmd=self.cmhr.corot_map_at_epoch(float(day))
      self.MG['flux'],self.MG['side']=cmd.resampleMapEquatorial(self.MG['eq_RA'],self.MG['eq_DEC'])
      hdulist[-1] = pyfits.ImageHDU(self.MG['flux'],name=str(day))

    thdulist=pyfits.HDUList(hdulist)
    fits_name = 'Zody_daily_fluxes_days%d-%d.fits'%(self.minday,self.maxday)
    thdulist.writeto(directory+fits_name,clobber=True)

  def calculate_flux_stats(self,daily_flux,directory='./'):
    
    import pyfits
    import numpy as np

    ff=pyfits.open(directory+daily_flux)
    # Remember that the first table in the file is the primary header
    cubic_matrix = np.zeros((len(ff)-1,ff[1].header['NAXIS2'],ff[1].header['NAXIS1']))

    for i in range(len(ff)-1):
      cubic_matrix[i]=ff[i+1].data

    hdulist = []
    hdulist.append(pyfits.PrimaryHDU())
    hdulist.append([])
    hdulist[-1] = pyfits.ImageHDU(cubic_matrix.mean(axis=0),name = 'flux_mean')
    hdulist.append([])
    hdulist[-1] = pyfits.ImageHDU(cubic_matrix.std(axis=0),name = 'flux_rms')
    hdulist.append([])
    hdulist[-1] = pyfits.ImageHDU(cubic_matrix.min(axis=0),name = 'flux_min')
    hdulist.append([])
    hdulist[-1] = pyfits.ImageHDU(cubic_matrix.max(axis=0),name = 'flux_max')
    
    thdulist=pyfits.HDUList(hdulist)
    fits_name = 'STATISTICS_'+daily_flux
    thdulist.writeto(directory+fits_name,clobber=True)

  def elongation_cut(self, map_to_cut, elong_map, directory='./'):

    import pyfits
    import numpy as np

    hdulist = []
    hdulist.append(pyfits.PrimaryHDU())
    
    cutted_matrix=np.zeros((180,360))
    
    elongs = pyfits.open(directory+elong_map)
    ff = pyfits.open(directory+map_to_cut)
    
    for ii in range(1,len(ff)):
      for row in range(180):
	good_idx = np.where((elongs[ii].data[row]>=self.elongmin)&(elongs[ii].data[row]<=self.elongmax))[0]
	cutted_matrix[row][good_idx] = ff[ii].data[row][good_idx]
      hdulist.append([])
      hdulist[-1] = pyfits.ImageHDU(cutted_matrix,name = ff[ii].header['EXTNAME'])
      
    thdulist=pyfits.HDUList(hdulist)
    fits_name = 'Zody_daily_fluxes_days%d-%d_CUT_elong%5.2f-%5.2f.fits'%(self.minday,self.maxday,self.elongmin,self.elongmax)
    thdulist.writeto(directory+fits_name,clobber=True)
    
  def create_image_mask(self, catalog, sourceID, ZLE_map, cat_directory = './catalog/', directory='./'):
    
    import pyfits
    import numpy as np
    import matplotlib.pyplot as plt

    cat = pyfits.open(cat_directory+catalog)
    selected = np.where(cat[1].data['NUMBER'] == sourceID)[0]
        
    try:
      ra = cat[1].data['RA'][selected]
      dec = cat[1].data['DEC'][selected]
    except:
      ra = cat[1].data['alpha'][selected]
      dec = cat[1].data['delta'][selected]

    # Bilinear interpolation to find the flux value    
    x1 = int(np.floor(ra))
    x2 = int(np.ceil(ra))
    y1 = int(np.floor(dec))
    y2 = int(np.ceil(dec))
    
    ZLE = pyfits.open(directory+ZLE_map)
    
    f11 = ZLE[1].data[y1][x1] 
    f12 = ZLE[1].data[y1][x2] 
    f21 = ZLE[1].data[y2][x1] 
    f22 = ZLE[1].data[y2][x2] 
    
    fx1 = (x2-ra)*f11+(ra-x1)*f12
    fx2 = (x2-ra)*f21+(ra-x1)*f22
    
    ZLE_flux = (y2-dec)*fx1+(dec-y1)*fx2
    # End of interpolation
    
    ff=pyfits.open(cat_directory+'images/000/image_%d.fits'%sourceID)
    src=ff[0].data

    mask=np.ones(src.shape)*ZLE_flux

    new_hdu=pyfits.PrimaryHDU(src+mask)

    fits_masked=pyfits.HDUList([new_hdu])
    new_file_name = 'masked_image_%d.fits'%sourceID
    
    for key in ff[0].header.keys():
      if key != 'COMMENT':
	new_hdu.header[key] = ff[0].header[key]
    
    fits_masked.writeto(cat_directory+'images/000/'+new_file_name,clobber=True)

    #plt.figure()

    #plt.subplot(121)
    #plt.imshow(src,cmap='jet',vmin=0,vmax=0.2,aspect='auto')
    #plt.colorbar()

    #plt.subplot(122)
    #plt.imshow(src+mask,cmap='jet',vmin=0,vmax=0.2,aspect='auto')
    #plt.colorbar()

    #plt.savefig('Test_ZLE_mask.png')
    
    
    
