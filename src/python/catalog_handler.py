import os
import numpy as np
import random
from fits_handler import fits2dict
from collections import OrderedDict

#from pyraf import iraf
#from iraf import rotate

#import fase
#import fase.system
#from fase.fileio import Catalog, Image

class CatalogHandler:

  def __init__(self, catalog, directory = './', ZEUS = False):
    #
    # initialize
    #
    self.ZEUS = ZEUS
    self.catalog_name = catalog
    self.catalog_dir = directory    
    self.catalog_dict = fits2dict(self.catalog_name,'SMALLCATALOG_CENTER_SIMM_R1',directory = self.catalog_dir)
    #self.__wcs = None
    #self.__orient = None
    
  def get_object(self, object_id):
    
    '''DEVO RISCRIVERCI DENTRO IL GETITEM DI Catalog.py, MODIFICATO AD UOPO'''
    data = OrderedDict()
    index = np.where(self.catalog_dict['num'] == object_id)[0]
    subdir = '000'
    #subdir = str(ident).zfill(7)[:3] 
    for col in self.catalog_dict.keys():
      data[col] = self.catalog_dict[col][index][0]
    if self.ZEUS == True:
      data['incident'] = self.catalog_dir+'incident/%s/masked_incident_%d.fits'%(subdir,object_id)        
      data['image'] = self.catalog_dir+'images/%s/masked_image_%d.fits'%(subdir,object_id)
    else:
      data['incident'] = self.catalog_dir+'incident/%s/incident_%d.fits'%(subdir,object_id)        
      data['image'] = self.catalog_dir+'images/%s/image_%d.fits'%(subdir,object_id)
    #if self.__wcs is None:
      #data['ximage'] = None
      #data['yimage'] = None      
    #else:  
      #ximage,yimage = self.__wcs.world2pix(float(data['alpha']),float(data['delta'])) 
      #data['ximage'] = ximage 
      #data['yimage'] = yimage        
    return data

  def allRange(self):
    """ Utility function to make the actual object selection"""
    
    N=4
    aux = np.concatenate((self.coord_indexes,self.mag_indexes,self.z_indexes,self.flux_indexes), axis=1)
    aux.sort() 
    shift = N-1
    self.indexes = (aux[aux[shift:] == aux[:-shift]],) 

  def coordRange(self, amin, amax, dmin, dmax):
    """ Select the objects inside a given coordinates range """

    self.amin = amin
    self.amax = amax
    self.dmin = dmin
    self.dmax = dmax

    alphas = self.catalog_dict['alpha']
    deltas = self.catalog_dict['delta']
    self.coord_indexes = np.where((alphas>=amin) & (alphas<=amax) & (deltas>=dmin) & (deltas<=dmax))[0]
    #self.allRange()
    
  def magRange(self, mmin, mmax):
    """ Select the objects inside a given magnitude range """
    
    mags = self.catalog_dict['mag']
    self.mag_indexes = np.where((mags>=mmin) & (mags<=mmax))[0]
    #self.allRange()

  def zRange(self, zmin, zmax):
    """ Select the objects inside a given redshift range """
    
    zs = self.catalog_dict['z']
    self.z_indexes = np.where((zs>=zmin) & (zs<=zmax))[0]
    #self.allRange()
    
  def fluxRange(self, fmin, fmax):
    """ Select the objects inside a given line flux range """
    
    fluxes = self.catalog_dict['fluxha']
    self.flux_indexes = np.where((fluxes>=fmin) & (fluxes<=fmax))[0]
    #self.allRange()

  #def setWCSFromImage(self, image):
    
    #self.__wcs = image
    
    
  #def setWCSFromData(self, alpha, delta, pix_size, nx, ny, roll=0.0):
    
    #if roll==90:
      #crpix1 = nx
      #crota2 = -90
    #else:
      #crpix1 = 1.0
      #crota2 = 0
    
    ##
    ## create the dummy WCS image
    ##
    #self.__wcs = Image([1,1])
    #self.__wcs['NAXIS1'] = 1
    #self.__wcs['NAXIS2'] = 1
    #self.__wcs['CRPIX1'] = crpix1
    #self.__wcs['CRPIX2'] = 1.0
    #self.__wcs['CRVAL1'] = alpha
    #self.__wcs['CRVAL2'] = delta
    #self.__wcs['CDELT1'] = pix_size/3600.
    #self.__wcs['CDELT2'] = pix_size/3600.
    #self.__wcs['CTYPE1'] = 'RA---TAN'
    #self.__wcs['CTYPE2'] = 'DEC--TAN'
    #self.__wcs['CROTA2'] = crota2
        
    #self.__roll = roll
  
