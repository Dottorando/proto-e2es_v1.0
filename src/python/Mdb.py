"""
File: Mdb.py

Created on: 05 28, 2014
Author: Sebastien Guilloux

"""
import xml.etree.cElementTree as ET


class Mdb(object):
    """
    Mdb parameters class.

    Use it to parse a MDB parameters xml file into a Python dictionary.
    Access its parameters using get and get_all method according to the
    parameter title and attributes.

    """
    def __init__(self, xmlfilenames, parameter_list=[]):
        """
        Mdb parameters class. This constructor will parse the given xml file.

        Args:
            xmlfilename:    Path to the xml file (MDBParameters) to parse,
                            or list of path if more than one is needed
            parameter_list: May contain parameter titles to filter the xml
                            file and keep only the given parameters.
                            An empty list means we parse the entire xml file.
        """
        self.parameters = {}
        """ parameters is a dictionary containing all parsed MDB parameters
            indexed by title."""
        # make parameter_list a set for faster checks later on
        parameter_set = set(parameter_list)
        if isinstance(xmlfilenames, basestring):
            xmlfilenames = [xmlfilenames]
        self._parse_files(xmlfilenames, parameter_set)

    def get(self, parameter_title, attribute='Value'):
        """
        Access the given attribute of the parameter called "parameter_title".

        Args:
            parameter_title:    Title of the parameter we want to access.
            attribute:          Attribute of the parameter we want to access.
                                Default to 'Value' which is the common usage.

        Examples:
            >> mdb.get('Environment.Constant.BoltzmannConstant')
            1.3806488e-23
            >> mdb.get('Environment.Constant.BoltzmannConstant', 'Expression')
            'MolarGasConstant / AvogadroConstant'
        """
        try:
            return self.parameters[parameter_title][attribute]
        except KeyError, e:
            msg = 'No such key in parsed MDB parameters: '+str(e)
            raise KeyError(msg)

    def get_all(self):
        """
        Returns the entire dictionary of parameters, indexed by title.
        """
        return self.parameters
    
    def get_size(self):
        """
        Returns the number of parameters in this Mdb instance
        """
        return len(self.parameters)
    
    def getValue(self, parameter_title):
        """
        Returns the value of the given parameter title
        """
        return self.get(parameter_title, attribute='Value')
    
    def getDescription(self, parameter_title):
        """
        Returns the description of the given parameter title
        """
        return self.get(parameter_title, attribute='Description')
    
    def getSource(self, parameter_title):
        """
        Returns the source of the given parameter title
        """
        return self.get(parameter_title, attribute='Source')
    
    def getExpression(self, parameter_title):
        """
        Returns the expression of the given parameter title
        """
        return self.get(parameter_title, attribute='Expression')
    
    def getRelease(self, parameter_title):
        """
        Returns the release of the given parameter title
        """
        return self.get(parameter_title, attribute='Release')
    
    def getUnit(self, parameter_title):
        """
        Returns the unit of the given parameter title
        """
        return self.get(parameter_title, attribute='unit')
    
    def getArrayUnits(self, parameter_title):
        """
        Returns a list of units of the array for the given parameter title
        such as ['m', 'sec', 'kg'].
        The list is ordered just as the MDB file.
        """
        available_units = ['xunit', 'yunit', 'zunit', 'uunit']
        result = []
        for unit in available_units:
            try:
                unit_res = self.get(parameter_title, attribute=unit)
                result.append(unit_res)
            except KeyError:
                pass
        return result
        

    #______Parsing private functions______

    def _parse_files(self, xmlfilenames, parameter_set):
        """
        Parse the given xml files Parameters.
        Filter on parameters title found in parameter_list.
        If parameter_list is empty, parse all parameters.
        """
        def check_parameter_list(title):
            """ Internal function to check if the given title is in the
            parameter_list.
            Returns True to retrieve all parameters if the list is empty. """
            if not parameter_set:
                return True
            else:
                return title in parameter_set
        for xmlfile in xmlfilenames:
            context = ET.iterparse(xmlfile, events=('start', 'end',))
            context = iter(context)
            in_param = False
            for event, elem in context:
                if (event == 'start' and elem.tag == 'Parameter' and
                   check_parameter_list(elem.get('title'))):
                        in_param = True
                elif (event == 'end' and elem.tag == 'Parameter' and
                      check_parameter_list(elem.get('title'))):
                        self._parse_parameter(elem)
                        in_param = False
                if not in_param:
                    elem.clear()

    def _parse_parameter(self, elem):
        """
        Parse the given parameter (ElementTree node)
        and add it to the parameters dictionary.
        """
        param = {}
        param['title'] = elem.get('title')
        param['unit'] = elem.get('unit')
        param['Description'] = elem.find('Description').text
        param['Source'] = elem.find('Source').text
        param['Expression'] = elem.find('Expression').text
        param['Release'] = elem.find('Release').text
        (value, additional_params) = self._parse_value(elem.find('Value'))
        param['Value'] = value
        # additional_params are merged with the param dictionary
        # eg. Xunit and Yunit for Array2D type
        if additional_params:
            for k in additional_params:
                param[k] = additional_params[k]
        # Add parsed param to the global parameters dict (indexed by title)
        self.parameters[param['title']] = param

    def _parse_value(self, elem):
        """ Parse the <Value> node """
        # Access the first child which is the actual value node
        val = elem[0]
        # Call convert_X where X is the value tag name (eg. Double, Integer...)
        return getattr(self, '_convert_'+val.tag)(val)

    #______Conversion private functions (one per type)______

    def _convert_Text(self, elem):
        return (elem.text, None)

    def _convert_Url(self, elem):
        l = elem.text.split('\n')
        l = [x.strip() for x in l]
        return (l, None)

    def _convert_Double(self, elem):
        return (float(elem.text), None)

    def _convert_Integer(self, elem):
        return (int(elem.text), None)

    def _convert_Boolean(self, elem):
        if elem.text.lower() == 'false' or elem.text == 0:
            return (False, None)
        else:
            return (True, None)

    def _convert_ListOfDouble(self, elem):
        # Numbers are separated by a whitespace in the xml node text
        l = [float(n) for n in elem.text.split(' ')]
        return (l, None)

    def _convert_ListOfInteger(self, elem):
        # Numbers are separated by a whitespace in the xml node text
        l = [int(n) for n in elem.text.split(' ')]
        return (l, None)

    def _convert_FitsFile(self, elem):
        # TODO: handle fully (using PyXB bindings?)
        filename = elem.findall('.//Filename')[0].text
        url = elem.findall('.//URL')
        if url:
            url = url[0].text
        return ({'filename': filename, 'url': url}, None)

    def _convert_Array(self, elem):
        # additional_params example: {'Xunit': 'deg', 'Yunit': 'Angstrom'}
        additional_params = elem.attrib
        arraysize_node = elem.find('SizeOfArray')
        additional_params['SizeOfArray'] = arraysize_node.text
        # Remove 'SizeOfArray' node to build array of meaningful values
        elem.remove(arraysize_node)
        array = []
        # One line per node, containing multiple values separated by whitespace
        for child in elem:
            array.append([float(n) for n in child.text.split(' ')])
        return (array, additional_params)

    def _convert_Array2D(self, elem):
        return self._convert_Array(elem)

    def _convert_Array3D(self, elem):
        return self._convert_Array(elem)

    def _convert_Array4D(self, elem):
        return self._convert_Array(elem)
