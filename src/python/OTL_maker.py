class OperationalTimeLine:
  
  def __init__(self, file_name):
    
    self.file_name=file_name
    
  def create_header(self):

    optl = open(self.file_name,'w+')
    header = 'pointingID,'
    header+= 'ra0,'
    header+= 'dec0,'
    header+= 'exptime,'
    header+= 'band,'
    header+= 'ZLE_wavelenght,'
    header+= 'min_day,'
    header+= 'max_day,'
    header+= 'elong_min,'
    header+= 'elong_max'

    optl.write(header+'\n')

    optl.close()    

  def add_pointing(self,ra0,dec0,exptime,band,ZLE_wavelength,min_day,max_day,elong_min,elong_max):
    
    optl = open(self.file_name,'r')
    lines = optl.readlines()
    optl.close()
    
    try:
      ID = int(lines[-1][0])+1
    except:
      ID = 1

    optl = open(self.file_name,'a')
    line = '%d,'%ID
    line+= '%f,'%ra0
    line+= '%f,'%dec0
    line+= '%f,'%exptime
    line+= '%s,'%band
    line+= '%f,'%ZLE_wavelength
    line+= '%d,'%min_day
    line+= '%d,'%max_day
    line+= '%f,'%elong_min
    line+= '%f,'%elong_max

    optl.write(line+'\n')

    optl.close()

  def del_pointing(self,pointingID):
    
    optl = open(self.file_name,'r')
    lines = optl.readlines()
    optl.close()
        
    for line in lines:
      if line.startswith(str(pointingID)):
	lines.remove(line)
    
    optl = open(self.file_name,'w+')
    for l in lines:
      optl.write(l)
    optl.close()
    
  def copy_pointing(self,pointingID):

    optl = open(self.file_name,'r')
    lines = optl.readlines()
    optl.close()
    
    for iline in range(len(lines)):
      if lines[iline].startswith(str(pointingID)):
	lines.append(lines[iline])
    
    optl = open(self.file_name,'w+')
    for l in lines:
      optl.write(l)
    optl.close()
