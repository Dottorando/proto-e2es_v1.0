import os
import sys

if len(sys.argv) < 3+1 :
   print "%s input_fits input_dat input_conf\n"%sys.argv[0]
   sys.exit(1)

input_fits=str(sys.argv[1]) # Multidrizzle/CMC150811_bright_1x1_rand_NISP_GRED0_00_IMG.fits
input_dat=str(sys.argv[2]) # input.dat
input_conf=str(sys.argv[3]) # conf/NISP_GRED0_00.conf

#AXE_BINS='/iraf/iraf/extern/stsdas/bin.linux/'
AXE_BINS='./src/aXe_executable/'
in_prefix = input_fits.split('.')[0]
out_prefix = in_prefix+'_2'

out_cat = out_prefix+'.cat'
out_CONT_fits = out_prefix+'.CONT.fits'
out_OAF = out_prefix+'.OAF'
out_PET_fits = out_prefix+'.PET.fits'
out_SPC_fits = out_prefix+'.SPC.fits'
#
# clean
#
clean_command = 'rm %s %s %s %s %s'%(out_cat,out_CONT_fits,out_OAF,out_PET_fits,out_SPC_fits)
os.system(clean_command)
# 
# 1. sex2gol 
#      => SIMAGE_2.cat
sex2gol_command = AXE_BINS+'aXe_SEX2GOL'+' '+input_fits+' '+input_conf+' '+'-in_SEX='+input_dat+' '+'-no_direct_image'
os.system(sex2gol_command)
# 
# 2. gol2af 
#      => SIMAGE_2.OAF
#
orient_value = 0
mfwhm_value = 1.0
gol2af_command = AXE_BINS+'aXe_GOL2AF'+' '+input_fits+' '+input_conf+' '+'-orient=%d'%orient_value+' '+'-mfwhm=%2.1f'%mfwhm_value
os.system(gol2af_command)
#
# 3. af2pet
#      => SIMAGE_2.PET.fits
#
af2pet_command = AXE_BINS+'aXe_AF2PET'+' '+input_fits+' '+input_conf
os.system(af2pet_command)
#
# 4. petcont
#      => (modifica) SIMAGE_2.PET.fits
#      => SIMAGE_2.CONT.fits
#
cont_model_value = 4
cont_map_value = 1
lambda_psf_value = 1220.0
petcont_command = AXE_BINS+'aXe_PETCONT'+' '+input_fits+' '+input_conf+' '+' '+'-cont_model=%d'%cont_model_value+' '+'-cont_map=%d'%cont_map_value+' '+'-lambda_psf=%5.1f'%lambda_psf_value
os.system(petcont_command)
#
# 5. petff
#      => senza FF probabilmente non fa nulla
#
back_value = 0
petff_command = AXE_BINS+'aXe_PETFF'+' '+input_fits+' '+input_conf+' '+'--back=%d'%back_value
os.system(petff_command)
#
# 6. pet2spc
#      => SIMAGE_2.SPC.fits
#
pet2spc_command = AXE_BINS+'aXe_PET2SPC'+' '+input_fits+' '+input_conf+' '+'-noBPET'
os.system(pet2spc_command)

