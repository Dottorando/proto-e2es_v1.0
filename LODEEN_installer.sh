#! /bin/bash

read -s -p 'Your password is required! PROVIDE! PROVIDE!' PASSWORD

sudo -k
if sudo -lS &> /dev/null << EOF
$PASSWORD
EOF
then
    echo
    echo 'Your password is correct!'
else 
    echo
    echo 'Your password is not correct! EXPLAIN! EXPLAIN!'
    exit
fi

echo
echo '== STARTING INSTALLATION =='
echo
echo '== TIPS INSTALLATION =='
echo

echo 'Compiling wcstools...'
echo
cp ./proto-E2ES_v1.0/tar_files/wcstools-3.9.2.tar.gz .
tar -zxf wcstools-3.9.2.tar.gz
rm wcstools-3.9.2.tar.gz
cd wcstools-3.9.2
make -s
echo

# Exit from wcstools-3.9.2 folder
cd

echo 'Compiling cfitsio...'
echo
cp ./proto-E2ES_v1.0/tar_files/cfitsio3370.tar.gz .
tar -zxf cfitsio3370.tar.gz
rm cfitsio3370.tar.gz
cd cfitsio
./configure -q
make -s
make install -s
echo

# Exit from cfitsio folder
cd

echo 'Installing asciidata...'
echo
cp ./proto-E2ES_v1.0/tar_files/asciidata-1.1.1.tar.gz .
tar -zxf asciidata-1.1.1.tar.gz
rm asciidata-1.1.1.tar.gz
cd asciidata-1.1.1
echo $PASSWORD | sudo -S python setup.py install
echo

# Exit from asciidata-1.1.1 folder
cd

echo 'Compiling TIPS...'
echo
cp ./proto-E2ES_v1.0/tar_files/tips_codeen.tar.gz .
tar -zxf tips_codeen.tar.gz
rm tips_codeen.tar.gz
cd ./tips_codeen/axesim
./configure -q --with-cfitsio-prefix=/home/user/cfitsio --with-wcstools-prefix=/home/user/wcstools-3.9.2
make -s
echo

# Exit from tips_codeen folder
cd

echo '== IRAF INSTALLATION =='
echo
echo $PASSWORD | sudo -S mkdir /iraf
echo $PASSWORD | sudo -S mkdir /iraf/iraf
echo $PASSWORD | sudo -S cp ./proto-E2ES_v1.0/tar_files/iraf.lnux.x86_64.tar.gz /iraf/iraf
cd /iraf/iraf
echo $PASSWORD | sudo -S tar -zxf iraf.lnux.x86_64.tar.gz
echo $PASSWORD | sudo -S rm iraf.lnux.x86_64.tar.gz
echo yes | sudo -S ./install
cd
echo $PASSWORD | sudo -S mkdir /iraf/x11iraf
echo $PASSWORD | sudo -S cp ./proto-E2ES_v1.0/tar_files/x11iraf-v2.0BETA-bin.linux.tar.gz /iraf/x11iraf
cd /iraf/x11iraf
echo $PASSWORD | sudo -S tar -zxf x11iraf-v2.0BETA-bin.linux.tar.gz
echo $PASSWORD | sudo -S rm x11iraf-v2.0BETA-bin.linux.tar.gz
echo /iraf/x11iraf sudo -S ./install
cd
cp ./proto-E2ES_v1.0/tar_files/ds9.linux64.7.2.tar.gz .
tar -zxf ds9.linux64.7.2.tar.gz
rm ds9.linux64.7.2.tar.gz
echo $PASSWORD | sudo -S mv ds9 /usr/local/bin
cp ./proto-E2ES_v1.0/tar_files/iraf .
echo $PASSWORD | sudo -S chmod u=rwx iraf
mkdir IRAF
cd IRAF
echo y echo xgterm | /usr/local/bin/mkiraf
cd
echo $PASSWORD | sudo -S cp ./proto-E2ES_v1.0/tar_files/iraf_extern.tar.gz /iraf/iraf
cd /iraf/iraf
echo $PASSWORD | sudo -S tar -zxf iraf_extern.tar.gz
echo $PASSWORD | sudo -S rm iraf_extern.tar.gz
cd
pwd
echo
echo '== PROTO-E2ES SETUP =='
echo
echo 'Unpacking proto-E2ES_v1.0...'
echo
echo 'Configuring environment...'
echo
cp ./IRAF/login.cl ./proto-E2ES_v1.0
cd ./proto-E2ES_v1.0
sed -i -e "s/dottorando/$USERNAME/" set_environment.source


echo '== INSTALLATION COMPLETED =='
echo








